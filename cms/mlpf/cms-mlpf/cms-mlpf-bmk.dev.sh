#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  sleep 5s 
  echo "Hello world" > out_$1.log 2>&1
  echo "[doOne ($1)] $(date) EXTRA_ARGS='$EXTRA_ARGS'"

  # parse extra args if any
  options=$(getopt -a -n cms-mlpf-bmk -o g:D:B: --long nepochs:,ntrain:,ntest:,nvalid:,batch-multiplier:,gpus:,dtype:nworkers: -- "$EXTRA_ARGS")
  eval set -- "$options"
  while [ : ]; do
    case "$1" in
      --ntrain ) NTRAIN="$2"; shift;;
      --ntest ) NTEST="$2"; shift;;
      --nvalid ) NVALID="$2"; shift;;
      --nepochs ) NEPOCHS="$2"; shift;;
      --nworkers ) NWORKERS="$2"; shift;;
      --gpus | -g ) NGPUS="$2"; shift;;
      --batch-multiplier | -B ) BMULT="$2"; shift;;
      --dtype | -D ) DTYPE="$2"; shift;;
      --train ) TRAIN="--train";;
      -- ) shift; break;;
    esac
    shift
  done


  # Run the workload
  python3 mlpf/pyg_pipeline.py ${TRAIN} \
  --config parameters/pytorch/pyg-clic.yaml \
  --benchmark_dir $resultsDir \
  --gpus $NGPUS \
  --batch_size $BSIZE \
  --num-epochs $NEPOCHS \
  --ntrain $NTRAIN \
  --ntest $NTEST \
  --nvalid $NVALID \
  #--prefix /tmp/train_ \
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=1 # cannot be changed by user input ()
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=1 # not relevant for GPUs
# specific to MLPF
NEPOCHS=6     # must be >1 as 1st epoch is thrown away
NTRAIN=120000 # 0 is None, events to train on
NTEST=36000   # 0 is None, events to test training
BSIZE=8       # 8 is Default, batch size (too small device is under-loaded, too large OOM)
NGPUS=1    # 0 is Default, GPUs
TRAIN="--train"
DEBUG=0
resultsDir="/results"

function usage_detailed(){
  echo ""
  echo "Additional MLPF parameters:"
  echo "     --nepochs                 : (int) Number of epochs >1 (default: $NEPOCHS)"
  echo "     --ntrain                  : (int) Train steps limit (default: $NTRAIN)"
  echo "     --ntest                   : (int) Test steps limit (default: $NTEST)"
  echo "     --nvalid                  : (int) Validation steps limit (default: $NVALID)"
  echo "     --batch_size              : (int) Batch size (default: $BSIZE)"
  echo "     --dtype                   : (string) Data type {float32, float16, bfloat16}(default: $DTYPE)"
  echo "  -B --batch-        : (int) Batch multiplier, 1=16G,5=80G GPU memory (default: $BATCH_MULTIPLIER)"
  echo "  -g --gpus                    : (int) Number of gpus to use (default: $NGPUS)"
  
}


if [ -f /run/.containerenv ]; then FLAVOR="podman"
elif [ -f /.dockerenv ]; then FLAVOR="docker"
elif [ -f /singularity ]; then FLAVOR="singularity"
else FLAVOR="unknown";
fi

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi


##############################


set -e


log() {
  case $1 in
    error)  shift 1; echo -e "\e[31m>>> ERROR:\e[0m $*\n" | tee -a $resultsDir/out.log ; exit 2 ;;
    info)   shift 1; echo -e "\e[34m$*\e[0m\n" | tee -a $resultsDir/out.log ;;
    silent) shift 1; echo "$*" >> $resultsDir/out.log ;;
    *)      echo "$*" | tee -a $resultsDir/out.log ;
  esac
}



# set CUDA_VISIBLE_DEVICES for tensorflow based on nvidia-smi (dirty nvidia-only check)
if type -P "nvidia-smi" &>/dev/null; then
  DEVICES=$(nvidia-smi -L | wc -l)
  log info "Detected $DEVICES nvidia GPUs"
  export CUDA_VISIBLE_DEVICES=$(seq -s, 0 $(($DEVICES-1)))
fi

# create /results/build to satisfy common build script (mimic bmk-driver.sh)
log silent "Creating /results/build"
mkdir -p $resultsDir/build
touch $resultsDir/build/.pointlessfile

log info "Running benchmark MLPF"
log silent "Executing 'python3 mlpf/pipeline.py train \
  --config parameters/delphes-benchmark.yaml \
  --prefix /tmp/train_ \
  --plot-freq 1000000 \
  --benchmark_dir $resultsDir \
  --num_devices $NDEVICES \
  --batch_size $BSIZE \
  --nepochs $NEPOCHS \
  --ntrain $NTRAIN \
  --ntest $NTEST'"
cd /bmk/cms-mlpf/particleflow/


REPORT=$(cat $resultsDir/result.json)

generate_json() {
  jq -n \
    --argjson nepochs "$NEPOCHS" \
    --argjson report "$REPORT" \
    --arg containment "$FLAVOR" \
    --arg description "$DESCRIPTION" \
    '{
      "run_info":{
        $nepochs
      },
      $report,
      "app":{
        $containment,
        $description
      }
    }'
}
mkdir -p $resultsDir/report
if [ $skipSubDir -eq 0 ]; then
  REPORT_PATH=$resultsDir/report/cms-mlpf_summary.json
else
  REPORT_PATH=$resultsDir/cms-mlpf_summary.json
fi
generate_json > $REPORT_PATH
log info "Finished running MLPF. Final report written to $REPORT_PATH"

# sourcing bmk-driver excluded for now pending rework to override common args
