# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

export CMSSW_RELEASE=CMSSW_12_5_0
export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
source $VO_CMS_SW_DIR/cmsset_default.sh
export SCRAM_ARCH=el8_amd64_gcc10
[[ ! -e ${CMSSW_RELEASE} ]] && scram project CMSSW ${CMSSW_RELEASE}
cd ${CMSSW_RELEASE}
eval `scramv1 runtime -sh`
cd ..
conddb -y copy 125X_mcRun3_2022_realistic_v3 --destdb GlobalTag.db

rm -rf ${CMSSW_RELEASE}
