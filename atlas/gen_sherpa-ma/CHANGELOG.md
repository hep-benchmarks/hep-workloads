# Changelog for Atlas gen_sherpa MultiArchitecture (MA)

## [v2.2] 2024-04-25
- A new result parser script, written in python, has been added. The old parser calculated the score based on the walltime, 
  which is dominated by initialization for small numbers of events. The new parser examines the timestamps in the log file to determine the beginning and end of the actual event loop. 
- Because the timestamps have resolution of 1 second, a minimum number of events (50) is now required. If fewer than 50 events are requested, the workload fails.
- The workload is single-threaded. Prior to this version, requests for multiple threads were silently ignored. Now, the workload will fail if the number of threads requested is not equal to one.

## [v2.1] 2023-11-10
- Limit the number of loaded cores using the argument -n | --ncores 

## [v2.0] 2022-10-13
- First working release of Atlas gen_sherpa built for x86_64 and aarch64
- The version starts from v2.0 to avoid confusion with the legacy images built in atlas/gen_sherpa
- This release is based on Athena 23.0.3 and is different w.r.t. the workload built in atlas/gen_sherpa that is based on Athena 21.6.84
