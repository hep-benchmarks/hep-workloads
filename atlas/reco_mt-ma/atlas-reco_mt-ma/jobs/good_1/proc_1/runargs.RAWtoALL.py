# Run arguments file auto-generated on Thu Nov  3 17:42:42 2022 by:
# JobTransform: RAWtoALL
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'RAWtoALL' 

runArgs.perfmon = 'fastmonmt'
runArgs.conditionsTag = 'CONDBR2-BLKPA-RUN2-09'
runArgs.autoConfiguration = ['everything']
runArgs.maxEvents = 12
runArgs.geometryVersion = 'ATLAS-R2-2016-01-00-01'
runArgs.preExec = ['rec.doTrigger=False']
runArgs.postExec = ["svcMgr.DBReplicaSvc.COOLSQLiteVetoPattern=''"]

 # Input data
runArgs.inputBSFile = ['/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/Tier0ChainTests/data17_13TeV.00330470.physics_Main.daq.RAW._lb0310._SFO-1._0001.data']
runArgs.inputBSFileType = 'BS'
runArgs.inputBSFileNentries = 1571
runArgs.BSFileIO = 'input'

 # Output data
runArgs.outputHIST_R2AFile = 'tmp.HIST_R2A'
runArgs.outputHIST_R2AFileType = 'hist_r2a'
runArgs.outputAODFile = 'myAOD.pool.root'
runArgs.outputAODFileType = 'AOD'

 # Extra runargs

 # Extra runtime runargs

 # Literal runargs snippets

 # Executor flags
runArgs.totalExecutorSteps = 0
