# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

parseResultsDir=$(cd $(dirname ${BASH_SOURCE}); pwd) # needed to locate parseResults.py

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  echo "[parseResults] current directory: $(pwd)"
  # #-----------------------
  # Parse results (bash)
  #-----------------------
  echo "[parseResults] bash parser starting"
  # Parsing  Event Throughput: xxxx ev/s

  grep_string="Time per event:"
  sed_string="s@[^:]*: -> Time per event: [0-9\.]*e[0-9\-]*s (\([0-9\.]*e[0-9\-\+]*\) events per hour.*@\1@"

  res_score=`grep -H "${grep_string}" proc_*/out_*.log | sed -e "${sed_string}" | awk 'BEGIN{sum=0;}  { val=$1/3600.; sum+=val; } END{printf "{\"gen\": %.4f}", sum}' || (echo "{}"; return 1)`
  STATUS_1=$?

  res_stat=`grep -H "${grep_string}" proc_*/out_*.log | sed -e "${sed_string}" | awk 'BEGIN{amin=1000000;amax=0;count=0;sum=0;}  { val=$1/3600.; a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} END{n = asort(a); if (n % 2) {   median=a[(n + 1) / 2]; } else {median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;}; printf "{\"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f, \"count\": %d}", sum/count, median, amin, amax, count}' || (echo "{}"; return 1)`
  STATUS_2=$?

  [[ "$STATUS_1" == "0" ]] && [[ "$STATUS_2" == "0" ]] 
  STATUS=$?
  echo "[parseResults] parsing completed (status=$STATUS)"
  [[ "$STATUS" != "0" ]] && return $STATUS
  #-----------------------
  # Generate summary
  #-----------------------
  echo "[parseResults] generate report"
  resJSON="{\"wl-scores\": $res_score, \"wl-stats\": $res_stat }"
  echo "$resJSON" > $baseWDir/parser_output.json  
  #-----------------------
  # Return status
  #-----------------------
  return $shstatus
}
