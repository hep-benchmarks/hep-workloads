import json
import glob
import os
from collections import defaultdict
from math import log10, floor

def round_to_significant_digits(value, digits):
    if not isinstance(value, (int, float)):
        return value
    if value == 0:
        return 0
    else:
        return round(value, digits - int(floor(log10(abs(value)))) - 1)

def process_value(value, significant_digits):
    print(value)
    if isinstance(value, dict):
        return {k: process_value(v, significant_digits) for k, v in value.items()}
    elif isinstance(value, (int, float)):
        return round_to_significant_digits(value, significant_digits)
    else:
        return value

def merge_dicts(dicts, significant_digits=5):
    """Merge multiple dictionaries with identical structure, creating lists at the leaf nodes."""
    def merge_values(val_list, significant_digits=5):
        if all(isinstance(v, dict) for v in val_list):
            # If all values are dictionaries, merge them recursively
            merged = {}
            keys = set(k for d in val_list for k in d)
            for key in keys:
                merged[key] = merge_values([d[key] for d in val_list if key in d])
            return merged
        else:
            # If the values are not dictionaries, return them as a list
            return [round_to_significant_digits(v, significant_digits) for v in val_list]

    return merge_values(dicts, significant_digits)

def parse_results(baseWDir, significant_digits=5):
    # Initialize variables to store cumulative results
    total_throughput = 0
    all_data = []

    # Loop through each JSON file and collect data
    for jsonFile in glob.glob(os.path.join(baseWDir, 'proc_*/flowsim_output_*.json')):
        with open(jsonFile, 'r') as f:
            data = json.load(f)
            total_throughput += data['performance']['throughput_objects_per_second']
            all_data.append(data)

    # Merge all collected data
    merged_results = merge_dicts(all_data, significant_digits)

    # Generate the JSON output
    resJSON = {
        "wl-scores": {
            "flowsim": round_to_significant_digits(total_throughput, significant_digits)
        },
        "wl-stats": process_value(merged_results, significant_digits)
    }

    # Write the JSON output to a file
    output_file = os.path.join(baseWDir, 'parser_output.json')
    with open(output_file, 'w') as f:
        json.dump(resJSON, f, indent=4)

# Example usage:
# parse_results('/path/to/baseWDir', significant_digits=5)

if __name__ == "__main__":
    import sys
    parse_results(sys.argv[1], significant_digits=5)
