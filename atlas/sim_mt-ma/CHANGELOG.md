# Changelog for Atlas simMT

## [Unreleased]

## [v2.1] 2023-12-19
- Limit the number of loaded cores using the argument -n | --ncores 
- Minor patch of the athena setup (BMK-1209)

## [v2.0] 2022-10-04
- First working release of Atlas simMT built for x86_64 and aarch64
- The version starts from v2.0 to avoid confusion with the legacy images built in atlas/sim_mt having version v1.x
- This release is based on Athena 23.0.3 and is different w.r.t. the workload built in atlas/sim_mt that is based on Athena 22.0.27