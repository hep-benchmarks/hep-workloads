#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, EXTRA_ARGS, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  arg1=$1; shift # LbEnv must be called without any arguments
  # Configure WL copy
  echo "[doOne ($arg1)] $(date) starting in $(pwd)"  
  workDir=$(pwd)
  cd .. # test that changing directory in doOne does not affect results (e.g in syncprocesses)
  echo "[doOne ($arg1)] EXTRA_ARGS='$EXTRA_ARGS'"
  status=0
  actions=
  for arg in $EXTRA_ARGS; do
    if [ "$arg" == "--hallo-world" ]; then
      if [ "${actions/1}" == "${actions}" ]; then actions="${actions} 1"; fi
    elif [ "$arg" == "--setup-lhcb" ]; then
      if [ "${actions/2}" == "${actions}" ]; then actions="${actions} 2"; fi
    elif [ "$arg" == "--setup-gauss" ]; then
      if [ "${actions/3}" == "${actions}" ]; then actions="${actions} 3"; fi
    else
      echo "ERROR! Invalid argument '$arg'"; status=1; break
    fi
  done
  if [ "$actions" == "" ]; then actions=2; fi # default is LHCb setup
  # Execute WL copy
  for action in $actions; do
    # Synchronise all processes to start the next action at the same time (BMK-1048)
    # Sleep at most for 50 steps of 0.1 seconds each, i.e. 5 seconds in total
    syncprocesses $arg1 --workDir $workDir --status $status --sleepstep 0.1 --sleepstepsleft 50
    status=${?}
    # Execute one action
    if [ "$status" != "0" ]; then
      # This process failed setup or a previous action, or it received ABORT from a failure in another process 
      echo "[doOne ($arg1)] $(date) current status=$status: skip next action '${action}'"
      ###break # breaks the action for-loop
    else
      echo "[doOne ($arg1)] $(date) current status=$status: execute next action '${action}'"
      if [ "$action" == "1" ]; then
        echo "Hallo World! (FASTER DUMMY TEST)" >${workDir}/out_$arg1.log 2>&1
      elif [ "$action" == "2" ]; then
	# Use LbEnv instead of LbLogin.sh which is no longer supported (BMK-1137)
	source /cvmfs/lhcb.cern.ch/lib/LbEnv >${workDir}/out_$arg1.log 2>&1 \
	  && echo "LHCb setup (DEFAULT DUMMY TEST)" >>${workDir}/out_$arg1.log 2>&1
      else
	source /cvmfs/lhcb.cern.ch/lib/LbEnv >${workDir}/out_$arg1.log 2>&1 \
	  && lb-run -c x86_64_v2-centos7-gcc11-opt --use="AppConfig v3r412" --use="Gen/DecFiles v32r2" --use="ProdConf" Gauss/v56r2 echo "LHCb Gauss setup (SLOWER DUMMY TEST)" >>${workDir}/out_$arg1.log 2>&1
      fi
      status=${?}
    fi
  done
  echo "[doOne ($arg1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function usage_detailed may be defined in each benchmark
# Input arguments: none
# Return value: none
function usage_detailed(){
  echo "NEVENTS_THREAD is ignored"
  echo
  echo "Optional EXTRA_ARGS can have at most one argument:"
  echo "  --hallo-world : dummy HalloWorld test (faster)"
  echo "  --setup-lhcb  : dummy LHCb setup test (DEFAULT)"
  echo "  --setup-gauss : dummy LHCb Gauss setup test (slower)"
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NCOPIES=$(nproc)
NEVENTS_THREAD=1 # ignored

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
