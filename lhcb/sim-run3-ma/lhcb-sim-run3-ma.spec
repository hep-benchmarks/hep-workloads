HEPWL_BMKEXE=lhcb-sim-run3-ma-bmk.sh
HEPWL_BMKOPTS="-c 4 -e 6" # NB: use 6 events in the CI (test BMK-991) even if the container uses 5 events as default
HEPWL_BMKDIR=lhcb-sim-run3-ma
HEPWL_BMKDESCRIPTION="LHCb SIM-RUN3 x86/ARM benchmark (Feb 2023)"
HEPWL_BMKOS="centos:centos7.9.2009" # as in hello-world-c7-ma.spec
HEPWL_BMKUSEGPU=0
HEPWL_DOCKERIMAGENAME=lhcb-sim-run3-ma-bmk
HEPWL_DOCKERIMAGETAG=v1.1 # NB: use ci-vX.Y for tests (can be rebuilt) and vX.Y for production (cannot be rebuilt)
HEPWL_CVMFSREPOS=lhcb.cern.ch,lhcb-condb.cern.ch,sft.cern.ch
HEPWL_BUILDARCH="x86_64,aarch64"
