#!/usr/bin/env python3

# Based on belle2 parseResults.py

import os
import json
import bilby
import numpy as np
from bilby.core.result import Result

Scores = []

bilby_label = os.environ['bilby_label']

# Each copy generates an output file
for i in range(0, int(os.environ['NCOPIES'])):
    outdir = "proc_" + str(i + 1) + "/result/"

    robj = Result.from_json(filename=outdir+bilby_label+"_result.json")
    nlikelihood = robj.meta_data['run_statistics']['nlikelihood']
    sampling_time = robj.sampling_time.total_seconds()
    del robj

    likelihoods_per_sec = nlikelihood/sampling_time
    Scores.append(likelihoods_per_sec)


AvgTotal = np.mean(Scores)
MedTotal = np.median(Scores)
MinTotal = np.min(Scores)
MaxTotal = np.max(Scores)

OutputJSON = {}
OutputJSON['wl-scores'] = {}
OutputJSON['wl-stats'] = {}

OutputJSON['wl-scores']['pe'] = AvgTotal * int(os.environ['NCOPIES'])

OutputJSON['wl-stats']['avg'] = AvgTotal
OutputJSON['wl-stats']['median'] = MedTotal
OutputJSON['wl-stats']['min'] = MinTotal
OutputJSON['wl-stats']['max'] = MaxTotal
OutputJSON['wl-stats']['count'] = int(os.environ['NCOPIES'])

basewdir = os.environ['baseWDir']

with open(basewdir+"/parser_output.json", "w") as OutputFile:
    json.dump(OutputJSON, OutputFile)

