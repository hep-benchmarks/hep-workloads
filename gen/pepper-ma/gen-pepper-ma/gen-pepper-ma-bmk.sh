#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  VAR_DOONE_1=$1
  VAR_DOONE_2=$2

  echo "[doOne (${VAR_DOONE_1})] $(date) starting in $(pwd)"

  NGPUS=0
  USE_TASKSET=0
  PEPPER_PROCESS="g_g__g_g"
  NUMBER_OF_BATCHES=4

  if [[ "EXTRA_ARGS" != "" ]]; then
    echo "[doOne (${VAR_DOONE_1})] EXTRA_ARGS=$EXTRA_ARGS"
    # parse extra args if any
    options=$(getopt -a -n gen-pepper-ma-bmk -o g:p:b: --long taskset,process:,gpus:,batches: -- $EXTRA_ARGS)
    echo "options $options"
    eval set -- "$options"
    while [[ $# -gt 0 ]]; do
      case "$1" in
        --taskset ) USE_TASKSET=1; shift;;
        --process | -p ) PEPPER_PROCESS="$2"; shift 2;;
        --gpus | -g ) NGPUS="$2"; shift 2;;
        --batches | -b ) NUMBER_OF_BATCHES="$2"; shift 2;;  # New batches parameter
        -- ) shift; break;;
        * ) break ;;  # Handle unexpected arguments safely
      esac
    done

    echo "[doOne (${VAR_DOONE_1})] USE_TASKSET=${USE_TASKSET}"
    echo "[doOne (${VAR_DOONE_1})] PEPPER_PROCESS=${PEPPER_PROCESS}"
    echo "[doOne (${VAR_DOONE_1})] NGPUS=${NGPUS}"
    echo "[doOne (${VAR_DOONE_1})] number_of_batches=${NUMBER_OF_BATCHES}"

  fi

  # Here start the process of grouping the cores from 0 to nproc-1
  # in groups of NTHREADS cores, in case the TASKSET option is used
  COPY=${VAR_DOONE_1}
  RANK=$((COPY - 1))
  LOWERCPU=$((RANK * NTHREADS))
  UPPERCPU=$((COPY * NTHREADS - 1))

  PEPPER_COMMAND="/usr/local/bin/pepper -p ${PEPPER_PROCESS} -b $(( $NEVENTS_THREAD * $NTHREADS)) -n ${NUMBER_OF_BATCHES} > out_${VAR_DOONE_1}.log 2>&1"
  
  # NB
  # --allow-run-as-root is needed because in singularity the user seen inside the container
  # is not the container's user (bmkuser) but the host user, that could be root 
  COMMAND="mpiexec --allow-run-as-root -n $NTHREADS ${PEPPER_COMMAND}"

  # optionally use taskset to constrain to physical CPU cores (only relevant when not using the full machine)
  if [[ "${USE_TASKSET}" == "1" ]]; then
    COMMAND="mpiexec --allow-run-as-root -n $NTHREADS taskset -c ${LOWERCPU}-${UPPERCPU} ${PEPPER_COMMAND}"
  fi
  
  echo "[doOne (${VAR_DOONE_1})] $(date) going to execute command: ${COMMAND}"
  eval ${COMMAND}
  status=${?}
  echo "[doOne (${VAR_DOONE_1})] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}


export PATH=/usr/lib64/openmpi/bin:$PATH
export LD_LIBRARY_PATH=/usr/lib64/openmpi/lib:$LD_LIBRARY_PATH

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NEVENTS_THREAD=10000
NTHREADS=4
NCOPIES=$(( `nproc` / $NTHREADS ))

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
