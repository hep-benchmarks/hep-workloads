#!/bin/bash

# Copyright 2019-2021 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"

  # Configure WL copy
  inputdatadir=/cvmfs/atlas.cern.ch/repo/benchmarks/hep-workloads/input-data
  inputdata=$inputdatadir/EVNT.13043099._000859.pool.root.1
  #echo "multiplicants"
  #echo ${NEVENTS_THREAD}
  #echo ${NTHREADS}
  maxEvents=$(($NEVENTS_THREAD*$NTHREADS))
  athenaCores=$NTHREADS
  echo $maxEvents

  # Extra ATLAS-specific setup
  release=23.0.3
  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
  asetup  Athena,${release} --tdaqarea=/cvmfs/atlas.cern.ch/repo/sw/tdaq
  unset FRONTIER_SERVER

  export ATHENA_CORE_NUMBER=$athenaCores # env variable for ATHENA threads/process number

  # Execute WL copy
  Sim_tf.py \
      --multithreaded=True \
      --inputEVNTFile="$inputdata" --maxEvents=${maxEvents} \
      --geometryVersion=ATLAS-R3S-2021-03-00-00 \
      --AMITag=s3873 \
      --postInclude="default:RecJobTransforms/UseFrontier.py" \
      --preInclude="EVNTtoHITS:Campaigns/MC21SimulationMultiBeamSpot.py,SimulationJobOptions/preInclude.ExtraParticles.py,SimulationJobOptions/preInclude.G4ExtraProcesses.py" \
      --firstEvent=6160001 --outputHITSFile=myHITS.pool.root --physicsList=FTFP_BERT_ATL \
      --randomSeed=6163 --conditionsTag=default:OFLCOND-MC21-SDR-RUN3-05 \
      --truthStrategy=MC15aPlus --DBRelease=300.0.4  --simulator=FullG4MT_QS --jobNumber=1 



  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Number of events per thread
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  # Number of copies and number of threads per copy
  if [ "$USER_NTHREADS" != "" ] && [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$USER_NTHREADS
  elif [ "$USER_NTHREADS" != "" ]; then
    NTHREADS=$USER_NTHREADS
    NCOPIES=$((`nproc`/$NTHREADS))
  elif [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$((`nproc`/$NCOPIES))
  fi
  # Return 0 if input arguments are valid, 1 otherwise
  # Report any issues to parseResults via s_msg
  export s_msg="ok"
  tot_load=$(($NCOPIES*$NTHREADS))
  if [ $tot_load -gt `nproc` ]; then
    s_msg="[ERROR] NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=$tot_load > number of available cores (`nproc`)"
    return 1
  elif [ $tot_load -eq 0 ]; then
    s_msg="[ERROR] NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=$tot_load. Please fix it"
    return 1
  elif [ $tot_load -ne `nproc` ];
  then s_msg="[WARNING] NCOPIES*NTHREADS ($NCOPIES*$NTHREADS=$tot_load) != `nproc` (number of available cores nproc)"
    echo $s_msg
  fi
  return 0
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NTHREADS=4
NCOPIES=$((`nproc`/$NTHREADS))
if [ "$NCOPIES" -lt 1 ]; then # when $NTHREADS > `nproc`
  NCOPIES=1
  NTHREADS=$((`nproc`/$NCOPIES))
fi
NEVENTS_THREAD=20

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
