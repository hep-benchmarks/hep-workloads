#!/bin/bash 

# Script to test the json result output when one of the parallel copies of a workload fails
# The failure is simulated killing a process
# The expected, correct output should include "wl-scores":{}'"

function run_kill(){
    IMAGE=$1
    ARGS=$2
    APPtoKill=$3

    echo "[$(date)] parameters $IMAGE , $ARGS , $APPtoKill"

    docker rm -f mytest_cont
    docker pull $IMAGE
    sleep 10 #needed otherwise sometimes the next command scrashes
    (docker run --rm --name mytest_cont -v /tmp/results:/results $IMAGE $ARGS -- ; 
        exit_status=$?; 
        echo "docker exit status ${exit_status}" > exit_status_$APPtoKill.txt;
    ) > /tmp/results/test_$APPtoKill.log &
    echo "docker process $dpid"
    sleep 20s
    echo -e "\nrunning processes"
    docker exec mytest_cont  ps -ef | grep $APPtoKill | grep -v 'grep'  
    echo " "
    pid=`docker exec mytest_cont ps -ef | grep $APPtoKill | grep -v 'grep' | head -1 | awk '{print $2}'`
    echo -e "\nkilling one process pid=$pid"
    docker exec mytest_cont kill -9 $pid
    docker exec mytest_cont  ps -ef | grep $APPtoKill | grep -v 'grep'  
    docker exec mytest_cont tail -f /results/test_$APPtoKill.log | grep -2 "\(lint json file\)\|\(exiting common benchmark driver\)"
    sleep 2
    cat exit_status_$APPtoKill.txt
    echo "[$(date)] END"
    [ `grep -c '"wl-scores":{\s*}' /tmp/results/test_$APPtoKill.log` -eq 0 ] && echo "PROBLEM, the APP did not report the FAILURE" && exit 1
    echo -e "\n-----------------\n"
}

function patch(){
    cat > Dockerfile << EOF
    FROM $IMAGE
    COPY $FILE $DEST
EOF
    cat Dockerfile
    docker build -t patched_image:latest -f Dockerfile /data/gitlab/vscode-hep-workloads/hep-workloads/
}


#-----------------------------------------------------------------------------

IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads/atlas-gen-bmk:v2.0
ARGS="-c2 -e3"
APPtoKill=athena

FILE=atlas/gen/atlas-gen/parseResults.sh
DEST=/bmk/atlas-gen
#patch $FILE $DEST
#IMAGE=patched_image:latest
#run_kill $IMAGE "$ARGS" $APPtoKill
[ `echo $IMAGE | grep -c gitlab` -gt 0 ] && docker rmi $IMAGE

#-----------------------------------------------------------------------------

IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads/atlas-sim-bmk:v2.0
ARGS="-c2 -e2"
APPtoKill=Sim_tf.py
#run_kill $IMAGE "$ARGS" $APPtoKill
[ `echo $IMAGE | grep -c gitlab` -gt 0 ] && docker rmi $IMAGE

#-----------------------------------------------------------------------------

IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads/cms-gen-sim-bmk:v2.0
ARGS="-c2 -e3 -t2"
APPtoKill=cmsRun
#run_kill $IMAGE "$ARGS" $APPtoKill
[ `echo $IMAGE | grep -c gitlab` -gt 0 ] && docker rmi $IMAGE

#-----------------------------------------------------------------------------

IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads/cms-digi-bmk:v2.0
ARGS="-c2 -e3"
APPtoKill=cmsRun
#run_kill $IMAGE "$ARGS" $APPtoKill
[ `echo $IMAGE | grep -c gitlab` -gt 0 ] && docker rmi $IMAGE

#-----------------------------------------------------------------------------

IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads/cms-reco-bmk:v2.0
ARGS="-c2 -e3"
APPtoKill=cmsRun
#run_kill $IMAGE "$ARGS" $APPtoKill
[ `echo $IMAGE | grep -c gitlab` -gt 0 ] && docker rmi $IMAGE

#-----------------------------------------------------------------------------

IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads/lhcb-gen-sim-bmk:v2.0
ARGS="-c2 -e2"
APPtoKill=lb-run
#run_kill $IMAGE "$ARGS" $APPtoKill

FILE=lhcb/gen-sim/lhcb-gen-sim/parseResults.sh
DEST=/bmk/lhcb-gen-sim
patch $FILE $DEST
IMAGE=patched_image:latest
run_kill $IMAGE "$ARGS" $APPtoKill
[ `echo $IMAGE | grep -c gitlab` -gt 0 ] && docker rmi $IMAGE

#-----------------------------------------------------------------------------

IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads/alice-gen-sim-bmk:v2.0
ARGS="-c2 -e3 -t1"
APPtoKill=athena
run_kill $IMAGE "$ARGS" $APPtoKill

[ `echo $IMAGE | grep -c gitlab` -gt 0 ] && docker rmi $IMAGE
