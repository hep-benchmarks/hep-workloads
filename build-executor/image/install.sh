#!/usr/bin/env bash 

set -ex

function install_x86(){
    # Instructions to install packages only for x86
    echo -e "\n[install_x86]\n"
    install_common
}

function install_aarch(){
    # Instructions to install packages only for x86
    echo -e "\n[install_aarch]\n"
    install_common
}

function install_common(){
    INSTALL_PKGS_A="
    epel-release \
    https://ecsft.cern.ch/dist/cvmfs/cvmfs-config/cvmfs-config-default-latest.noarch.rpm \
    https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm \
    util-linux  wget git \
    bc tar freetype perl jq unzip bzip2 \
    man man-pages file \
    yum-utils  
    ";

   
    #dnf-plugin-ovl
	
    yum install -y $INSTALL_PKGS_A
    yum repolist
    ls /etc/yum.repos.d/


    export INSTALL_PKGS_CUSTOM="
    https://ecsft.cern.ch/dist/cvmfs/cvmfs-2.9.4/cvmfs-2.9.4-1.$OS.$ARCH.rpm \
    https://ecsft.cern.ch/dist/cvmfs/cvmfs-2.9.4/cvmfs-shrinkwrap-2.9.4-1.$OS.$ARCH.rpm \
    http://mirror.ihep.ac.cn/yum/site/7/x86_64/cvmfs-config-ihep-default-1.6-1.noarch.rpm
    "; 

	
    yum install -y $INSTALL_PKGS_CUSTOM


    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo 
    
    INSTALL_PKGS_B="singularity \
    python3-pip \
    python2-pip \
    postfix \
    rsync \
    skopeo \
    bc \
    ShellCheck \
    mailx \
    lzo
    ";

    yum install -y $INSTALL_PKGS_B 

    yum install -y docker-ce 
    
    #yum -y --setopt=tsflags=nodocs install $INSTALL_PKGS 
    #rpm -V $INSTALL_PKGS 
    yum -y clean all --enablerepo='*'; 
    sed -e 's@inet_interfaces = localhost@#inet_interfaces = localhost@' -e 's@inet_protocols = all@inet_protocols = ipv4@' -i /etc/postfix/main.cf; 
    #pip2 install --upgrade pip; 
    python3 -m pip install --upgrade pip; 
    #pip2 install dictdiffer pyyaml numpy; 
    python3 -m pip install dictdiffer pyyaml numpy
}

INSTALL_SCRIPT=$(readlink -f $0)
SOURCE_DIR=$(readlink -f $(dirname $0))
echo "SOURCE_DIR $SOURCE_DIR"

cd ${SOURCE_DIR}
ls -l 

# Check O/S version
if [ ! -f /etc/redhat-release ]; then
    echo "ERROR! O/S is not a RedHat-based Linux system"
    echo "ERROR! This script is only supported on CentOS7 and CentOS8"
    exit 1
elif egrep -q "^CentOS Linux release 7" /etc/redhat-release; then
    export OS="el7"
    echo "INFO: Found OS $OS"
elif egrep -q "^CentOS Stream release 8" /etc/redhat-release; then
    export OS="el8"
    echo "INFO: Found OS $OS"
else
    echo "ERROR! This script is only supported on CentOS7 or CentOS8"
    exit 1
fi

ARCH=`uname -p`
echo "Detected CPU architecture $ARCH"
if [[ ${ARCH} =~ (^x86_(32|64)$) ]]; then
    echo "Installing $ARCH specific libraries"
    install_x86
elif [[ ${ARCH} =~ (^aarch64$) ]]; then
    echo "Installing $ARCH specific libraries"
    install_aarch
else
    echo "ERROR! This script is only supported on x86 and aarch64 architectures"
    exit 1
fi

