# Changelog for CMS HLT MultiArch

## [Unreleased]

## [v0.2] 2023-10-03
- Adapt to the new common approach to call prmon

## [v0.1] 2022-11-22
- First working release of CMS HLT for x86_64 and aarch64

