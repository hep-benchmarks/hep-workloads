# Changelog for LHCb sim-run3-ma

## [Unreleased]

## [v1.1] 2023-11-07
- Limit the number of loaded cores using the argument -n | --ncores

## [v1.0] 2023-02-17
- Production version of LHCb simulation based on 2023 
- Enable ARM run
