This workload executes the 'bilby_pipe_analysis' stage of a
bilby gravitational-wave parameter estimation workflow. This
instance uses nested sampling (using the 'dynesty' sampler) with
an unreasonably high value of 'dlogz' to ensure that the process
completes in a reasonable time.  This has been run on a simulated
signal injected into Gaussian noise.  Full details may be found
in the accompanying auxfiles/config_complete.ini file.

This analysis uses the IMRPhenomXPHM waveform. 
Each instance is a multi-threaded application running on **four** cores (fix value); 
the default number of copies is the number of cores divided by 4.
There is not concept of events per thread, 
therefore this parameter is arbitrary set to 1 and not used.

The relevant figure-of-merit (FOM) is likelihood evaluations per
second (LPS).  The total number of LPS is reported in 'wl-scores'.
Detailed statistics are reported in 'wl-stats' and contain the
mean, median, maximum, minimum, and total number of copies run.

Full current documentation for the bilby pipeline may be found at:

https://lscsoft.docs.ligo.org/bilby/index.html

The code is hosted publicly at:

https://git.ligo.org/lscsoft/bilby

It is released under the MIT license, as documented in the
LICENSE.md file at the above repository.
