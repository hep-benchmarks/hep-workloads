#!/bin/bash
#set -e # immediate exit on error

parseResultsDir=$(cd $(dirname ${BASH_SOURCE}); pwd) # needed to locate parseResults.py

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  echo "[parseResults] current directory: $(pwd)"
  #-----------------------
  # Parse results (python)
  #-----------------------
  echo "[parseResults] parseResultsDir ${parseResultsDir}"
  PYTHONPATH=${parseResultsDir} python ${parseResultsDir}/parseResults.py
  pystatus=$?

  return $pystatus
}

