#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Extra ATLAS-GEN-specific setup
  release=23.0.3
  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
  asetup  Athena,${release} --tdaqarea=/cvmfs/atlas.cern.ch/repo/sw/tdaq
  
  # Configure WL copy
  #joboptions=$BMKDIR/auxfiles/mc.Sh_TtllTest.py
  # Execute WL copy
  Gen_tf.py --ecmEnergy="13000"  --firstEvent="1" --jobConfig="$BMKDIR/auxfiles" \
    --outputEVNTFile=test_sherpa_tttautau.EVNT.pool.root --maxEvents="$((NEVENTS_THREAD*NTHREADS))" > out_$1.log 2>&1
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"

  # Number of copies and number of threads per copy
  if [ "$USER_NTHREADS" != "" ] && [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$USER_NTHREADS
  elif [ "$USER_NTHREADS" != "" ]; then
    NTHREADS=$USER_NTHREADS
    NCOPIES=$((`nproc`/$NTHREADS))
  elif [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=1
  fi  

  if (( $NTHREADS != 1 )); then
    echo "Invalid number of threads requested. Workload is single-threaded."
    return 1
  fi

  # Number of events per thread
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi

  if (( $NEVENTS_THREAD < 50 )); then
    echo "Invalid number of events requested. Workload must run at least 50 events to ensure an accurate score"
    return 1
  fi

  USETASKSET=0
  for arg in $EXTRA_ARGS; do
    [ "${arg}" == "--taskset" ] && USETASKSET=1
  done 

  echo "[validateInputArguments] USETASKSET=${USETASKSET}"

  # Return 0 if input arguments are valid, 1 otherwise
  return 0 # no checks
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=500

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
