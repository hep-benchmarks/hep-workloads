HEPWL_BMKEXE=gen-pepper-ma-bmk.sh
HEPWL_BMKOPTS="-c 2 -t 2 -m none --extra-args '--process=g_g__g_g --taskset -b 4'"
HEPWL_BMKDIR=gen-pepper-ma
HEPWL_BMKDESCRIPTION="Monte Carlo Generator Pepper"
HEPWL_DOCKERIMAGENAME=gen-pepper-ma-bmk
HEPWL_DOCKERIMAGETAG=ci-v1.0
HEPWL_CVMFSREPOS=NONE
HEPWL_EXTEND_SFT_SPEC=""
HEPWL_BMKOS="gitlab-registry.cern.ch/linuxsupport/alma9-base:20250108-1"
HEPWL_BUILDARCH="x86_64,aarch64"
