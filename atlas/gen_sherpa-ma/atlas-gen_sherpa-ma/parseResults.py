#!/usr/bin/python

import os
import json 
import datetime

#print("########################PARSERESULTS.PY########################")

GenScores = []
MaxGen = 0
AvgGen = 0
TotalGen = 0
MedGen = 0
MinGen = 1000000

# Used to check leap year
curyear = datetime.datetime.now().year

monthmap = {"Jan": 1, "Feb": 2, "Mar": 3, "Apr": 4, "May": 5, "Jun": 6, "Jul": 7, "Aug": 8, "Sep": 9, "Oct": 10, "Nov": 11, "Dec": 12}
monthdays = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}

# What if somebody is running this benchmark on Feb. 29, 2400? I don't want the score to be wrong!
if (curyear % 4 == 0 and curyear % 100 != 0) or (curyear % 400 == 0):
    monthdays[2] = 29

# Each copy generates an output file
for i in range(0, int(os.environ['NCOPIES'])):
    filename = "proc_" + str(i + 1) + "/log.generate"
    Gen = 0
    firstline = True
    timeflag = False

    with open(filename) as out:
        for line in out:
            if firstline:
                splitline = line.split()
                starttime = splitline[0]

                starthour = int(starttime.split(":")[0])
                startminute = int(starttime.split(":")[1])
                startsecond = int(starttime.split(":")[2])

                startmonth = splitline[2]
                # Convert month to number
                startmonth = monthmap[startmonth]

                startday = int(splitline[3])
                startyear = int(splitline[-1])

                firstline = False

            if "event number: 0" in line:
                timestr = line.split()[0]
                hour = int(timestr.split(":")[0])
                minute = int(timestr.split(":")[1])
                second = int(timestr.split(":")[2])

                # We've crossed a day boundary
                if hour < starthour:
                    # We've crossed a month boundary
                    if startday == monthdays[startmonth]:
                        # Who the hell runs benchmarks on NYE?
                        if startmonth == 12:
                            month = 1
                            day = 1
                            year = startyear + 1
                        else:
                            year = startyear
                            month = startmonth + 1
                            day = 1
                    else:
                        year = startyear
                        month = startmonth
                        day = startday + 1
                else:
                    year = startyear
                    month = startmonth
                    day = startday

                time1 = datetime.datetime(year, month, day, hour, minute, second)

            if "stopRun" in line:
                timeflag = True

            # Lots of lines contain "Time:", but the one immediately following stopRun is the one we want
            if timeflag and "Time:" in line:
                splitline = line.split()
                endtime = splitline[-2]

                endhour = int(endtime.split(":")[0])
                endminute = int(endtime.split(":")[1])
                endsecond = int(endtime.split(":")[2])

                endmonth = splitline[-4]
                # Convert month to number
                endmonth = monthmap[endmonth]

                endday = int(splitline[-3])
                endyear = int(splitline[-1])

                time2 = datetime.datetime(endyear, endmonth, endday, endhour, endminute, endsecond)

                timeflag = False


        seconds = (time2 - time1).total_seconds()
    
        Gen = float(os.environ['NEVENTS_THREAD']) / seconds
        GenScores.append(Gen)

        if Gen > MaxGen:
            MaxGen = Gen

        if Gen < MinGen:
            MinGen = Gen

        TotalGen = TotalGen + Gen 

AvgGen = TotalGen / float(os.environ['NCOPIES'])
GenScores.sort()

if len(GenScores) % 2 == 0:
    MedGen = (GenScores[int(len(GenScores) / 2)] + GenScores[int(len(GenScores) / 2 - 1)]) / 2.0
else:
    MedGen = GenScores[int(len(GenScores) / 2)]


OutputJSON = {}
OutputJSON['run_info'] = {}
OutputJSON['report'] = {}
OutputJSON['report']['wl-scores'] = {}
OutputJSON['report']['wl-stats'] = {}
OutputJSON['report']['wl-stats']['gen'] = {}
OutputJSON['app'] = {}

OutputJSON['run_info']['copies'] = int(os.environ['NCOPIES'])
OutputJSON['run_info']['threads_per_copy'] = int(os.environ['NTHREADS'])
OutputJSON['run_info']['events_per_thread'] = int(os.environ['NEVENTS_THREAD'])
OutputJSON['report']['wl-scores']['gen'] = AvgGen * int(os.environ['NCOPIES'])

OutputJSON['report']['wl-stats']['gen']['avg'] = AvgGen
OutputJSON['report']['wl-stats']['gen']['median'] = MedGen
OutputJSON['report']['wl-stats']['gen']['min'] = MinGen
OutputJSON['report']['wl-stats']['gen']['max'] = MaxGen
OutputJSON['report']['wl-stats']['gen']['count'] = int(os.environ['NCOPIES'])

# Borrowed from atlas/gen/atlas-gen/parseResults.py 
try:       
    with open(os.path.join(os.environ['BMKDIR'],"version.json")) as f:         
        version_json = json.loads(f.read())         
        version_json['containment'] = os.environ['flavor']
        OutputJSON["app"] = version_json
except:       
    OutputJSON["app"] = ''


# 2021-11-20 After moving parser logic into parser-driver
# only the WL specific stats are expected
# therefore the log is not added 
#OutputJSON['report']['log'] = os.environ['s_msg']

basewdir = os.environ['baseWDir']
OutputFile = open(basewdir+"/parser_output.json", "w")
json.dump(OutputJSON['report'], OutputFile)
OutputFile.close()

