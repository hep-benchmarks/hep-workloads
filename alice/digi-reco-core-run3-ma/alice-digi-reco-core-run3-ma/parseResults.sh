#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

parseResultsDir=$(cd $(dirname ${BASH_SOURCE}); pwd) # needed to locate parseResults.py

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  export res_tot='{}'
  export res_digi='{}'
  export res_reco='{}'

  
  #-----------------------
  # Parse results
  #-----------------------
  echo "[parseResults] parsing results from" proc_*/out_*.log
  # Parsing  Event Throughput: xxxx ev/s
  data_list=`grep "**** Pipeline done " proc_*/out_*.log | grep "(global_runtime :"| sed -e 's@.*global_runtime : \([0-9]*\.[0-9]*\)s.*@\1@' `
  res_tot=`compute_stat "$data_list"`
  #echo $res_tot
  STATUS_1=$?

  data_list=`grep "digi time" proc_*/out_*.log | grep "active fraction"| sed -e 's@.* time \([0-9]*\.\?[0-9]*\)s;.*@\1@'  `
  res_digi=`compute_stat "$data_list"`
  #echo $res_digi
  STATUS_2=$?

  data_list=`grep "reco time" proc_*/out_*.log | grep "active fraction"| sed -e 's@.* time \([0-9]*\.\?[0-9]*\)s;.*@\1@'  `
  res_reco=`compute_stat "$data_list"`
  #echo $res_reco
  STATUS_3=$?

  [[ "$STATUS_1" == "0" ]] && [[ "$STATUS_2" == "0" ]] && [[ "$STATUS_3" == "0" ]]
  STATUS=$?
  echo "[parseResults] parsing completed (status=$STATUS)"
  [[ "$STATUS" != "0" ]] && return $STATUS
  #-----------------------
  # Generate summary
  #-----------------------
  echo "[parseResults] generate report"
  # rearrange dictionary
  wl_scores=`get_wl_score "{\"digi-reco\":$res_tot , \"digi\":$res_digi, \"reco\":$res_reco}"`
  wl_stats=`get_wl_stats "{\"digi-reco\":$res_tot , \"digi\":$res_digi, \"reco\":$res_reco}"`

  echo "{"\"wl-scores\": ${wl_scores}, \"wl-stats\": ${wl_stats}"}" > $baseWDir/parser_output.json
  #-----------------------
  # Return status
  #-----------------------
  # Return 0 if result parsing and json generation were successful, 1 otherwise
  return $STATUS
}

function get_wl_score(){
  echo "$1" | jq 'with_entries(.value |= .score)'
}

function get_wl_stats(){
  echo "$1" | jq 'with_entries(.value |= .stats)'
}


function compute_stat(){
  echo $@ | tr " " "\n"| awk -v nevents=$(($NEVENTS_THREAD * $NTHREADS)) '
    BEGIN{amin=1000000;amax=0;count=0;}
    { if ($1>0) {val=( int(nevents*1.) / int($1*1.) ); a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} }
    END{
        sep=sprintf("%*s", 120, "");
        gsub(/ /, "*", sep);n=asort(a);
        if (n % 2) {
          median=a[(n + 1) / 2];
        } else {
          median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;
        }; 
        if (count<1) {exit 1}  # Fail if there are not entries
        mean_val=sum/count
        printf "{\"score\": %.4f , \"stats\": {\"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f, \"count\": %d}}", sum, mean_val, median, amin, amax, count
    }' || (echo "{}"; return 1)

}
