HEPWL_BMKEXE=mg5amc-madgraph4gpu-2022-bmk.sh

# Build both the CPP and CUDA benchmarks using a single CPU core (BMK-1018)
# Build four physics processes: skip ggttggg as ptxas build fails (BMK-1036, BMK-1037)
# Build both for float and double (single and double precision, BMK-1037)
# Build both with and without aggressive inlining in C++ code (BMK-1037)
# [NB: the default when running the container with no extra arguments is '--both -ggttgg -dbl -flt -inl0']
#HEPWL_BMKOPTS="-e 2 -c 1 --extra-args '--both -eemumu -ggtt -ggttg -ggttgg -dbl -flt -inl0 -inl1'" 

HEPWL_BMKOPTS="-e 2 -c 1 --extra-args '--both -ggtt -dbl -inl0'"

HEPWL_BMKDIR=mg5amc-madgraph4gpu-2022
HEPWL_BMKDESCRIPTION="Madgraph5_aMCatNLO madgraph4gpu benchmark (2022)" # NB the '@' character breaks sed scripts, do not use it!
HEPWL_BMKOS="gitlab-registry.cern.ch/linuxsupport/alma9-base:20230801-1"
HEPWL_BMKUSEGPU=1
HEPWL_DOCKERIMAGENAME=mg5amc-madgraph4gpu-2022-bmk
HEPWL_DOCKERIMAGETAG=ci-v0.10 # NB: use ci-vX.Y for tests (can be rebuilt) and vX.Y for production (cannot be rebuilt)
HEPWL_CVMFSREPOS=NONE
HEPWL_BUILDARCH="x86_64"
