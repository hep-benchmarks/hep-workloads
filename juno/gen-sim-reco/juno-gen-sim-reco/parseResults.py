#!/usr/bin/python

import sys
import os
import json
from typing import DefaultDict 

#print("########################PARSERESULTS.PY########################")

SimScores = []
RecoScores = []
TotalScores = []

MaxSim = 0
AvgSim = 0
MedSim = 0
MinSim = 1000000

MaxReco = 0
AvgReco = 0
MedReco = 0
MinReco = 1000000

MaxTotal = 0
AvgTotal = 0
MedTotal = 0
MinTotal = 1000000

def event_time(filename):
	try:
		print("Opening log file %s" % filename)
		file = open(filename,'r')
	except IOError:
        	error = []
	        return error
	content = file.read()
	file.close()
	return float(content.strip())

 
# Each copy generates an output file
for i in range(0, int(os.environ['NCOPIES'])):
#for i in range(0,1):
	simtime = "proc_" + str(i + 1) + "/outputsim"
	rectime = "proc_" + str(i + 1) + "/outputrec"

	# The parser reports separately the time taken by event generation, simulation, trigger simulation, 
	# and reconstruction (and the total). Times are reported from many sub-modules; these keywords 
	# are used to make the groupings
	#keywords = ["EvtGenInput", "Sum_Simulation", "Sum_TriggerSimulation", "SoftwareTrigger"]
	#keepgoing = False;
	#startintwo = False;

	Sim = 0
	Reco = 0
	Total = 0

	Sim = event_time(simtime)
	Reco = event_time(rectime)
	Total = Sim + Reco

	# Output is in seconds; convert to events / second
	Sim = float(os.environ['NEVENTS_THREAD']) / Sim
	Reco = float(os.environ['NEVENTS_THREAD']) / Reco
	Total = float(os.environ['NEVENTS_THREAD']) / Total

	print ("Sim: " + str(Sim))
	print ("Reco: " + str(Reco))
	print ("Total: " + str(Total))

	SimScores.append(Sim)
	RecoScores.append(Reco)
	TotalScores.append(Total)

	print (SimScores)
	print (RecoScores)
	print (TotalScores)

	if Sim > MaxSim:
		MaxSim = Sim
	if Reco > MaxReco:
		MaxReco = Reco
	if Total > MaxTotal:
		MaxTotal = Total

	if Sim < MinSim:
		MinSim = Sim
	if Reco < MinReco:
		MinReco = Reco
	if Total < MinTotal:
		MinTotal = Total

	AvgSim = AvgSim + Sim / float(os.environ['NCOPIES'])
	AvgReco = AvgReco + Reco / float(os.environ['NCOPIES'])
	AvgTotal = AvgTotal + Total / float(os.environ['NCOPIES'])


SimScores.sort()
RecoScores.sort()
TotalScores.sort()

if len(SimScores) % 2 == 0:
	MedSim = (SimScores[int(len(SimScores) / 2)] + SimScores[int(len(SimScores) / 2 - 1)]) / 2.0
	MedReco = (RecoScores[int(len(RecoScores) / 2)] + RecoScores[int(len(RecoScores) / 2 - 1)]) / 2.0
	MedTotal = (TotalScores[int(len(TotalScores) / 2)] + TotalScores[int(len(TotalScores) / 2 - 1)]) / 2.0
else:
	MedSim = SimScores[int(len(SimScores) / 2)]
	MedReco = RecoScores[int(len(RecoScores) / 2)]
	MedTotal = TotalScores[int(len(TotalScores) / 2)]


OutputJSON = {}
OutputJSON['wl-scores'] = DefaultDict(dict)
OutputJSON['wl-stats'] =  DefaultDict(dict)

OutputJSON['wl-scores']['sim'] = AvgSim * int(os.environ['NCOPIES'])
OutputJSON['wl-scores']['reco'] = AvgReco * int(os.environ['NCOPIES'])
OutputJSON['wl-scores']['gen-sim-reco'] = AvgTotal * int(os.environ['NCOPIES'])

OutputJSON['wl-stats']['gen-sim-reco']['avg'] = AvgTotal
OutputJSON['wl-stats']['gen-sim-reco']['min'] = MinTotal
OutputJSON['wl-stats']['gen-sim-reco']['max'] = MaxTotal
OutputJSON['wl-stats']['gen-sim-reco']['count'] = int(os.environ['NCOPIES'])
OutputJSON['wl-stats']['sim']['avg'] = AvgSim
OutputJSON['wl-stats']['sim']['min'] = MinSim
OutputJSON['wl-stats']['sim']['max'] = MaxSim
OutputJSON['wl-stats']['sim']['count'] = int(os.environ['NCOPIES'])
OutputJSON['wl-stats']['reco']['avg'] = AvgReco
OutputJSON['wl-stats']['reco']['min'] = MinReco
OutputJSON['wl-stats']['reco']['max'] = MaxReco
OutputJSON['wl-stats']['reco']['count'] = int(os.environ['NCOPIES'])


OutputFile = open("parser_output.json", "w")
json.dump(OutputJSON, OutputFile)
OutputFile.close()
