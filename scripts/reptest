#!/usr/bin/python
##############################################################
#
# reptest.py - HEP Workload Benchmarks Reproducibility Tests
# Chris Hollowell <hollowec@bnl.gov>
#
# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

import sys, getopt, string, os, subprocess, time, json, glob, math

RUNS = 50
REGISTRY = 'gitlab-registry.cern.ch/hep-benchmarks/hep-workloads'

def help():

    global RUNS

    print "reptest {-sd} [-v] [-r RUNS] [-L | -R REGISTRY] OUTPUTDIR BENCHMARK [BENCHMARK_OPTIONS] ...}"
    print "reptest -h"
    print "Option overview:"
    print "-h           Print help information and exit"
    print "-v           Display verbose output, including all scores"
    print "-d           Run Docker test"
    print "-s           Run Singularity test"
    print "-r RUNS      Number of runs to execute (default " + str(RUNS) + ")"
    print "-R REGISTRY  Use an alternate (instead of official) Docker registry"
    print "-L           Run container via local Docker daemon (cannot be used with -s)"
    print "\nExamples"
    print "--------"
    print "Run 'lhcb-gen-sim' 10 times in Docker and Singularity, displaying all scores:"
    print "reptest -sdv -r 10 /tmp/hwoutput lhcb-gen-sim"
    print "Run 'atlas-sim-bmk' 5 times in Docker:"
    print "reptest -d -r 5 /tmp/hwoutput atlas-sim-bmk"


def procres(rpath, verbose):
    
    global RUNS

    gpaths=glob.glob(rpath+"/*/*summary.json")    

    # Scores reported in the JSON output as 'throughput_score' or 'CPU_score', 
    # depending on the benchmark run - report both if available
    scores = { 'throughput_score': [], 'CPU_score': [] }
    found1 = False
    for sk in scores.keys():

        print sk + ":"
        for gpath in gpaths:
            jfile = open(gpath, mode='r')
            line = jfile.readline()
            jfile.close()
    
            jscore=json.loads(line)

            if sk not in jscore.keys():
                continue
        
            score = jscore[sk]['score'] 
            if verbose:
                print score
            try:
                float(score)
            except ValueError:
                print "\nError: invalid score for one or more runs!"
                sys.exit(2)
            scores[sk].append(score)

        if len(scores[sk])==0:
            print "\tNot reported\n"
        elif len(scores[sk])!=RUNS:
            print "\nError: missing json score file for one or more runs!"
            sys.exit(2)
        else:
            found1 = True

            average_score = sum(scores[sk]) / len(scores[sk])
            min_score = min(scores[sk])
            max_score = max(scores[sk])

            print "Avg: " + str(average_score)
            print "Min: " + str(min_score)
            print "Max: " + str(max_score)
            print "Spread (max-min) / average: " + str(round(((max_score - min_score) / average_score ) * 100, 2)) + "%"
            print "Min spread (max-min) / min: " + str(round(((max_score - min_score) / min_score ) * 100, 2)) + "%"

            devs = []
            for score in scores[sk]:
                dev = score - average_score
                dev = dev*dev
                devs.append(dev)
        
            stddev = math.sqrt(sum(devs) / len(devs))
            print "Std deviation: " + str(stddev)
            print "Relative std deviation: " + str(round((stddev / average_score) * 100, 2)) + "%\n"
   
    if not found1:
        print "Error: no scores found!"
        sys.exit(2)

def reptest(cm, output, benchmark, runlocal, verbose):
   
    global REGISTRY, RUNS

    commands = { 'docker': "docker run --network=host -v " + output + ":/results ",
                 'singularity': "singularity run -B " + output + ":/results docker://" }

    if not runlocal:
        if REGISTRY[-1] != '/':
            REGISTRY = REGISTRY + '/'
        benchmark = REGISTRY + benchmark

    print "Executing " + str(RUNS) + " " + cm + " runs of: " + benchmark
    print "Output: " + output
    print "System: " + ' '.join(os.uname())
    print "Date: " + time.asctime()

    try:
        lfile = open(output + "/reptest.log", mode='w')
    except:
        print "\nError: failure to open " + output + "reptest.log"

    for i in range(RUNS):
        sys.stdout.write('.')
        sys.stdout.flush()
        command = commands[cm] + benchmark

        try:
            cmdf = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) 
        except:
            print "\nError: failure to execute: " + command
            sys.exit(2)
        
        line = cmdf.stdout.readline()
        while line:
            lfile.write(line)
            lfile.flush()
            line=cmdf.stdout.readline()
  
    lfile.close()

    print "\n"

    procres(output, verbose)


def main():

    global REGISTRY, RUNS

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hvdsr:R:L')
    except getopt.GetoptError as err:
        print "\nError: " + str(err) + "\n"
        help()
        sys.exit(1)

    cms = []
    runlocal = False
    altreg = False
    verbose = False
    for opt,arg in opts:

        if opt == '-h':
            help()
            sys.exit(0)
        elif opt == '-r':
            try:
                RUNS = int(arg)
            except ValueError:
                help()
                sys.exit(1)
        elif opt == '-d':
            cms.append("docker")
        elif opt == '-v':
            verbose = True
        elif opt == '-s':
            cms.append("singularity")
        elif opt == '-L':
            runlocal = True
        elif opt == '-R':
            altreg = True
            REGISTRY = arg
            print REGISTRY

    if len(cms) < 1:
        print "\nError: must specify run type (Docker and/or Singularity)\n"
        help()
        sys.exit(1)

    if runlocal and (("singularity" in cms) or altreg):
        print "\nError: Local run requested with Singularity or alternate registry specification\n"
        help()
        sys.exit(1)

    if len(args) < 2:
        help()
        sys.exit(1)
    else:
        output = args[0]
        if not os.path.isdir(output):
            print "\nError: output directory must exist"
            sys.exit(1)
        benchmark = ' '.join(args[1:])

    benchmark_short = args[1].split(':')[0]

    # Create time-named subdirectory
    output = output + '/' + benchmark_short + '_' + time.strftime("%d%b%Y_%H%M%S")
    try:
        os.mkdir(output)
    except:
        print "\nError: failed to create " + output
        sys.exit(2)

    for cm in cms:
        noutput = output + '/' + cm
        if not os.path.isdir(noutput):
            try:
                os.mkdir(noutput)
            except:
                print "\nError: failed to create " + noutput
                sys.exit(2)
        reptest(cm, noutput, benchmark, runlocal, verbose)


if __name__ == '__main__':
    main()
