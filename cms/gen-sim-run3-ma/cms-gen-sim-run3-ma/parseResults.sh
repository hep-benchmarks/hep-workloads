# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

parseResultsDir=$(cd $(dirname ${BASH_SOURCE}); pwd) # needed to locate parseResults.py

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  echo "[parseResults] current directory: $(pwd)"
  export nevt_thr='-1'
  export res_cpu='{}'
  export res_thr='{}'
  export res_score='{}'
  #-----------------------
  # Parse results
  #-----------------------
  echo "[parseResults] parsing results from" proc_*/out_*.log
  # Documentation of cmssw time report at https://github.com/cms-sw/cmssw/blob/09c3fce6626f70fd04223e7dacebf0b485f73f54/FWCore/Services/plugins/Timing.cc#L240
  # Parsing Number of Events
  nevt_thr=`grep -h "Number of Events" proc_*/out_*.log | sed -e "s@.*:\([ 0-9\.]*\).*@\1@" | awk 'BEGIN{n=0; count=0;} {n+=$1; count+=1} END{print n/count/nthreads}' nthreads=$NTHREADS || (echo "-1"; return 1)`
  STATUS_1=$?

  # Parsing  Event Throughput: xxxx ev/s
  res_thr=`grep -H "Event Throughput" proc_*/out_*.log | sed -e "s@[^:]*: Event Throughput: \([ 0-9\.]*\) ev/s@\1@" | awk 'BEGIN{amin=1000000;amax=0;count=0;}  { val=$1; a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} END{n = asort(a); if (n % 2) {   median=a[(n + 1) / 2]; } else {median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;}; printf "{\"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f, \"count\": %d}", sum/count, median, amin, amax, count}' || (echo "{}"; return 1)`
  STATUS_2=$?

  #Duplicating above parsing, as quick and dirty. Should be replaced by a python parser
  res_score=`grep -H "Event Throughput" proc_*/out_*.log | sed -e "s@[^:]*: Event Throughput: \([ 0-9\.]*\) ev/s@\1@" | awk 'BEGIN{amin=1000000;amax=0;count=0;}  { val=$1; a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} END{n = asort(a); if (n % 2) {   median=a[(n + 1) / 2]; } else {median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;}; printf "{\"gen-sim\": %.4f}", sum}' || (echo "{}"; return 1)`
  STATUS_3=$?

  # Parsing  CPU Summary: \n- Total loop:: xxxx seconds of all CPUs
  res_cpu=`grep -H -A2 "CPU Summary" proc_*/out_*.log | grep "Total loop" | sed -e "s@.*\sTotal loop: \([ 0-9\.]*\)@\1@" | awk 'BEGIN{amin=1000000;amax=0;count=0;}  { val=nevt*nthreads/$1; a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} END{n = asort(a); if (n % 2) {median=a[(n + 1) / 2]; } else {median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;}; printf "{\"avg\": %.4f, \"median\": %.4f, \"min\": %.4f, \"max\": %.4f, \"count\": %d}", sum/count, median, amin, amax, count}' nevt=$nevt_thr nthreads=$NTHREADS || (echo "{}"; return 1)`
  STATUS_4=$?

  [[ "$STATUS_1" == "0" ]] && [[ "$STATUS_2" == "0" ]] && [[ "$STATUS_3" == "0" ]] && [[ "$STATUS_4" == "0" ]]
  STATUS=$?
  echo "[parseResults] parsing completed (status=$STATUS)"
  [[ "$STATUS" != "0" ]] && return $STATUS
  #-----------------------
  # Generate summary
  #-----------------------
  echo "[parseResults] generate report"
  NEVENTS_THREAD=${nevt_thr} # assign to NEVENTS_THREAD that goes to generateSummary the effective number of events per thread processed
  resJSON="{\"wl-scores\": $res_score, \"wl-stats\": {\"throughput_score\": $res_thr , \"CPU_score\": $res_cpu }}"
  echo $resJSON > $baseWDir/parser_output.json
  #-----------------------
  # Return status
  #-----------------------
  return $STATUS
}
