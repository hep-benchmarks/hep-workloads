# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.
#
# Run this script to regenerate GlobalTag.db
#

export CMSSW_RELEASE=CMSSW_12_5_0
export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
source $VO_CMS_SW_DIR/cmsset_default.sh
export SCRAM_ARCH=el8_amd64_gcc900
[[ ! -e ${CMSSW_RELEASE} ]] && scram project CMSSW ${CMSSW_RELEASE}
cd ${CMSSW_RELEASE}
eval `scramv1 runtime -sh`
cd ..
# Add the relevant information using the syntax:
#   conddb -y copy <db> --destdb GlobalTag.db
conddb -y copy 125X_mcRun3_2022_realistic_v3 --destdb GlobalTag.db
conddb -y copy L1TMuonGlobalParamsPrototype_Stage2v0_hlt --destdb GlobalTag.db
rm -rf ${CMSSW_RELEASE}
