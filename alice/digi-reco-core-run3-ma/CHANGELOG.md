# Changelog for Alice digi-reco-core-run3-ma

## [Unreleased]

## [v3.0] 2024-05-07
- Change the behaviour of the -e option to control the number of timeframes, rather than the number of events processed within a timeframe. The number of events per thread within a timeframe is now fixed to three signal and three background, and the number of threads is fixed to four. Requesting a number of threads different than four will cause the workload to fail. There is sufficient input data for five timeframes. Requesting a number of timeframes greater than five will cause the workload to fail.

## [v2.2] 2023-11-13
- Limit the number of loaded cores using the argument -n | --ncores 

## [v2.1] 2023-02-13
- Fix socket conflicts

## [v2.0] 2023-01-31
- First working release of Alice digi reco run3 for x86_64 and aarch64

