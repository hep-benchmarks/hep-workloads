#! /bin/sh
# DBRelease setup
echo Setting up DBRelease /cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/400.2.442 environment
export DBRELEASE=400.2.442
export CORAL_AUTH_PATH=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/400.2.442/XMLConfig
export CORAL_DBLOOKUP_PATH=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/400.2.442/XMLConfig
export TNS_ADMIN=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/400.2.442/oracle-admin
DATAPATH=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/400.2.442:$DATAPATH
# Customised environment
athena.py --preloadlib=$ATLASMKLLIBDIR_PRELOAD/libintlc.so.5:$ATLASMKLLIBDIR_PRELOAD/libimf.so --drop-and-reload --threads=4 runargs.RAWtoALL.py RecJobTransforms/skeleton.RAWtoALL_tf.py
