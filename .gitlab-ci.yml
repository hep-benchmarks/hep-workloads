# Copyright 2019-2023 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

stages:
- build-hepwlbuilder
- build-multiarch-manifest
- test-hepwlbuilder
- test-parsers
- build-hepwl
- build-multiarch-hepwl
- publish_oras
- test-singularity-image
- announce
- openMr

########################################
# Open a merge request
########################################

openMr:
  stage: openMr
  image: $CI_REGISTRY_IMAGE/hep-workload-builder-multiarch:latest
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa.*$/
  variables:
    TARGET_BRANCH: master
    TMP_BRANCH: tmp_${CI_COMMIT_BRANCH}_${CI_COMMIT_SHORT_SHA}
  script:
      - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' starting at $(date)***\n"
      - export TARGET_BRANCH
      - export TMP_BRANCH
      - source ${CI_PROJECT_DIR}/build-executor/utility.sh
      - openMR
      - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' finished at $(date)***\n"
  artifacts:
    paths:
      - /WL_list.md
      
announce:
  stage: announce
  image: $CI_REGISTRY_IMAGE/hep-workload-builder-multiarch:latest
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa.*$/
  script:
      - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' starting at $(date)***\n"
      - source ${CI_PROJECT_DIR}/build-executor/utility.sh
      - announce_standalone_image
      - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' finished at $(date)***\n"

########################################
# Publish singularity image with oras
########################################

.publish_oras:
  stage: publish_oras
  image: $CI_REGISTRY_IMAGE/hep-workload-builder-multiarch:latest
  tags:
    - hep-benchmarks
    - x86_64
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa-build.*$/
  variables:
    CIENV_PROCESS_ARCH: "all"
  script:
      - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' starting at $(date)***\n"
      - export SINGULARITY_CACHEDIR="/scratch/singularity/cache/cache_ci_pipeline-${CI_PIPELINE_ID}"
      - export SINGULARITY_TMPDIR="/scratch/singularity/tmp/tmp_ci_pipeline-${CI_PIPELINE_ID}"
      - source ${CI_PROJECT_DIR}/build-executor/utility.sh
      - publish_oras
      - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' finished at $(date)***\n"
  after_script:
    - ./gitlab-ci.sh clean

publish_oras_x86:
  variables:
    CIENV_PROCESS_ARCH: "x86_64"
  extends: .publish_oras

publish_oras_aarch64:
  variables:
    CIENV_PROCESS_ARCH: "aarch64"
  tags:
    - hep-benchmarks
    - aarch64  
  extends: .publish_oras

########################################
# test singularity image
########################################

.test-sing-img:
  image: $CI_REGISTRY_IMAGE/hep-workload-builder-multiarch:latest
  stage: test-singularity-image
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa-build.*$/
  script:
      - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' starting at $(date)***\n"
      - export SINGULARITY_CACHEDIR="/scratch/singularity/cache/cache_ci_pipeline-${CI_PIPELINE_ID}"
      - export SINGULARITY_TMPDIR="/scratch/singularity/tmp/tmp_ci_pipeline-${CI_PIPELINE_ID}"
      - source ${CI_PROJECT_DIR}/build-executor/utility.sh
      - test_singularity_from_registry
      - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' finished at $(date)***\n"

test-sing-img-x86:
  needs: ["publish_oras_x86"]
  tags:
    - hep-benchmarks
    - x86_64
    - no-gpu
  extends: .test-sing-img
  artifacts:
    paths:
      - x86_image_folder_dump.txt

test-sing-img-aarch64: 
  needs: ["publish_oras_aarch64"]
  tags:
    - hep-benchmarks
    - aarch64
    - no-gpu
  extends: .test-sing-img

test-sing-img-x86-gpu:
  needs: ["publish_oras_x86"]
  tags:
    - hep-benchmarks
    - x86_64
    - gpu
  extends: .test-sing-img
  artifacts:
    paths:
      - x86_image_folder_dump.txt

########################################
### Template for all the build jobs of hep-workloads containers
########################################

.build-hepwl-template:
  image: $CI_REGISTRY_IMAGE/hep-workload-builder-multiarch:latest
  stage: build-hepwl
  tags:
    - hep-benchmarks
  variables:
    # This is needed because of the GPU build arg
    JOBBUILDARG: build
    CIENV_BUILDIMG: $CI_REGISTRY_IMAGE/hep-workload-builder-multiarch:latest
  script:
    - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' starting at $(date)***\n"
    - source ${CI_PROJECT_DIR}/build-executor/utility.sh
    - if find_specfile >& /dev/null; then export CIENV_HEPWL_SPECFILE=$(find_specfile); else echo "ERROR! find_specfile failed (BMK-1158)"; exit 1; fi
    - echo "[CI script] spec file variable CIENV_HEPWL_SPECFILE=${CIENV_HEPWL_SPECFILE}"
    - source_specfile
    - run_procedure
    - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' finished at $(date)***\n"
  after_script:
    - ./gitlab-ci.sh clean
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa-build-.*$/
  artifacts:
    paths:
      - $CI_PROJECT_DIR/results.tar.gz
    expire_in: 1 week
    when: always

.build-hepwl-gpu-template:
  tags:
    - hep-benchmarks
    - x86_64
    - gpu
  variables:
    # This is needed because of the GPU build arg
    JOBBUILDARG: buildgpu
    CIENV_BUILDIMG: $CI_REGISTRY_IMAGE/hep-workload-builder-multiarch:latest
  extends: .build-hepwl-template

build-x86_64:  
  tags:
    - hep-benchmarks
    - x86_64
    - no-gpu
  extends: .build-hepwl-template

build-aarch64:   
  tags:
    - hep-benchmarks
    - aarch64
    - no-gpu
  extends: .build-hepwl-template

build-x86_64_GPU:  
  extends: .build-hepwl-gpu-template

# Enable when runner will be available
.build-aarch64_GPU:   
  tags:
    - hep-benchmarks
    - aarch64
    - gpu
  extends: .build-hepwl-gpu-template 
########################################
# Build multiarch
########################################

build-multiarch-hepwl:
  stage: build-multiarch-hepwl
  tags:
    - hep-benchmarks
    - x86_64
  image: # NB enable shared runners and do not specify a CI tag
     name: gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-builder/dind:v1.0
     entrypoint: [""]
  script:
    - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' starting at $(date)***\n"
    - source ${CI_PROJECT_DIR}/build-executor/utility.sh
    - create_multiarch_docker_manifest
    - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' finished at $(date)***\n"
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa-build-.*$/

###
### When a new build-image is created run the following tests to verify that hep-workload images can be built
### Run the build of a dummy CI test image
###

job_test-hepwlbuilder-ci: 
  variables:
    CIENV_HEPWL_SPECFILE: $CI_PROJECT_DIR/test/ci/test-ci.spec
  only:
    refs:
      - qa
    changes:
      - gitlab-ci.*
      - build-executor/*
      - build-executor/*/*
      - common/bmk-driver.sh
      - common/Dockerfile.*
      - test/ci/*
      - test/ci/*/*
  extends: .build-hepwl-template
  stage: test-hepwlbuilder


###########################################
### Template for all the build jobs of hep-workloads containers
###########################################

job_test-parsers:
  image: $CI_REGISTRY_IMAGE/hep-workload-builder-multiarch:latest
  stage: test-parsers
  script:
   - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' starting at $(date)***\n"
   - aline=`printf '=%.0s' {1..100}`;
   - for aparser in `find . -name "test_parser.sh"`; do echo -e "$aline\nRunning $aparser\n$aline"; jobdir=$(dirname $(readlink -f $aparser))/jobs; $aparser || (tar -cvzf archive.tgz $jobdir > /dev/null; exit 1); done
   - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' finished at $(date)***\n"
  #after_script:
  # - tar -cvzf archive.tgz .
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa.*$/
  artifacts:
    paths:
      - $CI_PROJECT_DIR/archive.tgz
    expire_in: 1 week
    when: on_failure

########################################
# Build Image Builder MULTIARCH
########################################

# Build the hep-workload-builder image (which is used to build and test WLs using docker)
# Use new CERN ci-tools (July 2019, BMK-113): see https://cern.service-now.com/service-portal/article.do?n=KB0005851
# Adapted from https://gitlab.cern.ch/gitlabci-examples/build_docker_image/blob/master/.gitlab-ci.yml
# [NB With the old ci-tools, the build cache seems to be ignored all the time on shared runners, i.e. yum install always runs]
# Kaniko is the recommended method for running Docker-builds as of May 2019.
# See https://docs.gitlab.com/ee/ci/docker/using_kaniko.html for GitLab's documentation.

build-hepwlbuilder-x86:
  stage: build-hepwlbuilder
  image: # NB enable shared runners and do not specify a CI tag
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder # CERN version of the Kaniko image
    entrypoint: [""]
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa$/
    changes:
      - build-executor/image/*
  variables:
    ARCHTAG: x86_64
  tags:
    - hep-benchmarks
    - x86_64
  script:
    - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' starting at $(date)***\n"
    # Prepare Kaniko configuration file
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # Build and push the image from the Dockerfile at the root of the project.
    # To push to a specific docker tag, amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
    # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
    - /kaniko/executor --context $CI_PROJECT_DIR/build-executor/image --dockerfile $CI_PROJECT_DIR/build-executor/image/Dockerfile  --destination $CI_REGISTRY_IMAGE/hep-workload-builder-multiarch:${ARCHTAG}-${CI_COMMIT_SHORT_SHA}
    - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' finished at $(date)***\n"

build-hepwlbuilder-aarch64:
  tags: 
    - hep-benchmarks
    - aarch64
  image: # NB enable shared runners and do not specify a CI tag
    name: gitlab-registry.cern.ch/hep-benchmarks/hep-spec/docker-image-builder:arm64-v1.3.0 # ARM version of the Kaniko image
  variables:
    ARCHTAG: aarch64
  extends: build-hepwlbuilder-x86

build_multiarch_manifest:
  stage: build-multiarch-manifest
  #needs: ["build-hepwlbuilder-x86","build-hepwlbuilder-aarch64"]
  tags:
    - hep-benchmarks
  image: # NB enable shared runners and do not specify a CI tag
     name: gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-builder/dind:v1.0
     entrypoint: [""]
  script:
    - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' starting at $(date)***\n"
    - IMAGE_NAME=hep-workload-builder-multiarch
    - X86_IMAGE=$CI_REGISTRY_IMAGE/${IMAGE_NAME}:x86_64-${CI_COMMIT_SHORT_SHA}
    - ARM_IMAGE=$CI_REGISTRY_IMAGE/${IMAGE_NAME}:aarch64-${CI_COMMIT_SHORT_SHA}
    - MULTIARCH_IMAGE=$CI_REGISTRY_IMAGE/${IMAGE_NAME}:${CI_COMMIT_SHORT_SHA}
    - echo "current commit is ${CI_COMMIT_SHORT_SHA}"
    - echo "current branch is ${CI_COMMIT_BRANCH}"
    - echo "current tag is ${CI_COMMIT_TAG}"
    - echo "X86_IMAGE=${X86_IMAGE}"
    - echo "ARM_IMAGE=${ARM_IMAGE}"
    - mkdir ~/.docker
    - echo "{\"experimental\":\"enabled\"}" > ~/.docker/config.json
    - echo $CI_BUILD_TOKEN | docker login -u gitlab-ci-token --password-stdin gitlab-registry.cern.ch
    - docker manifest create ${MULTIARCH_IMAGE} --amend ${X86_IMAGE} --amend ${ARM_IMAGE}
    - docker manifest push --purge ${MULTIARCH_IMAGE}
    - MULTIARCH_IMAGE=$CI_REGISTRY_IMAGE/${IMAGE_NAME}:latest
    - docker manifest create ${MULTIARCH_IMAGE} --amend ${X86_IMAGE} --amend ${ARM_IMAGE}
    - docker manifest push --purge ${MULTIARCH_IMAGE}
    - echo -e "\n*** [.gitlab-ci.yml] stage '${CI_JOB_STAGE}' finished at $(date)***\n"
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^qa$/
    changes:
      - build-executor/image/*

#############################################################################################
