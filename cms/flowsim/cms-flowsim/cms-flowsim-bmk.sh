#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  echo "[doOne ($1)] $(date) workDir is $workDir"
  workDir=$(pwd)

  #FIXME: This is done only because the FlowSim application looks for data to edit in FlowSim-benchmark/data
  # We shoudl make this folder configurable and to point to the workDir
  cp -r /bmk/FlowSim-benchmark ${workDir}


  status=1
 
  python3 ${workDir}/FlowSim-benchmark/inference.py \
    --model-path /bmk/data/checkpoint-latest.pt \
    --config-path /bmk/FlowSim-benchmark/configs/CRT_bigger.yaml \
    --source-file /bmk/data/gen_ttbar_2M_final-001.npy \
    --num-threads $NTHREADS \
    --output-json ${workDir}/flowsim_output_$1.json \
    ${EXTRA_ARGS} 2>&1 | tee -a out_$1.log 
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}


# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)

# function validateInputArguments(){
#   if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
#   echo "[validateInputArguments] validate input arguments"
#   return 0
# }


# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=1 #$(nproc)
NTHREADS=4 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=1 #FIXME: it means nothing now. could be used for the number of objects

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
