# Copyright 2019-2022 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

if [ "$BASH_SOURCE" = "$0" ]; then echo "ERROR! This script ($0) was not sourced"; exit 1; fi
if [ "$BASH_SOURCE" = "" ]; then echo "ERROR! This script was not sourced from bash"; return 1; fi

bmkDriver=$(basename ${BASH_SOURCE})
bmkScript=$(basename $0)
BMKDIR=$(cd $(dirname $0); pwd)

# Date with nanoseconds
function dateNS(){
  date +'%a %d %b %Y %H:%M:%S.%N %Z'
}

# Date with nanoseconds since 01-01-1970
function dateNSepoch(){
  date +'%s%N'
}

# Synchronise all processes to start the next action at the same time (BMK-1048)
function syncprocesses(){
  if [ "${9}" == "" ] || [ "${10}" != "" ] || [ "${2}" != "--workDir" ] || [ "${4}" != "--status" ] || [ "${6}" != "--sleepstep" ] || [ "${8}" != "--sleepstepsleft" ]; then # NB! Use "${10}", not "$10" which is interpreted as "${1}0"!
    echo "[doOne ($1): syncprocesses] INTERNAL ERROR! Invalid arguments '$@' to syncprocesses"
    echo "[doOne ($1): syncprocesses] Usage: syncprocesses <processid> --workDir <workDir> --status <status> --sleepstep <sleepstep (seconds)> --sleepstepsleft <sleepstepsleft (#steps)>"
    return 1
  fi
  workDir=$3 # expect absolute path ending with proc_$1 (not checked yet...)
  status=$5
  sleepstep=$7 # e.g. 0.1 means 100 ms
  sleepstepsleft=$9 # e.g. 50 means 5 seconds for 0.1 steps (bash only does integer arithmetics)
  sleepstepsdone=0
  ###echo "[doOne ($1): syncprocesses] $(dateNS) semaphore initializing for a new action '$0 $@' in $workDir"
  echo "[doOne ($1): syncprocesses] $(dateNS) semaphore invoked for a new action in $workDir"
  if [ ! -d $workDir ]; then
    echo "[doOne ($1): syncprocesses] $(dateNS) INTERNAL ERROR! directory $workDir not found"
    return 1
  fi
  if [ "$status" == "0" ]; then
    touch ${workDir}/semaphore.WAIT
    if [ -f ${workDir}/semaphore.RUNNING ]; then
      echo "[doOne ($1): syncprocesses] $(dateNS) semaphore.RUNNING: previous action has successfully completed, now WAIT"
      \rm -f ${workDir}/semaphore.RUNNING
    else
      echo "[doOne ($1): syncprocesses] $(dateNS) semaphore initializing (no previous action was RUNNING): WAIT for the GO signal"
      nwait=$(ls -1 ${workDir}/../proc_*/semaphore.WAIT 2> /dev/null | wc -l)
      echo "[doOne ($1): syncprocesses] $(dateNS) ${nwait} processes out of $NCOPIES are currently in the WAIT state"
    fi
  fi
  while [ $sleepstepsleft -gt 0 ] && [ "$status" == "0" ]; do
    # The FSM for one process has four states: WAIT (completed ok), GO (received go), RUNNING (started), ABORT (failed)
    # All processes are initially in the WAIT state (unless one already failed and is in ABORT)
    # When one process is in the ABORT state, the semaphore master (process 1) changes them all into the ABORT state
    # When all processes are in the WAIT state, the semaphore master (process 1) changes them all into the GO state
    # A process in the WAIT state, with more actions to run, sleeps until it receives the GO (or ABORT) signal from master
    # A process in the WAIT state, with no more actions to run, exits with status=0
    # A process in the GO state starts processing the action and enters the RUNNING state
    # A process in the RUNNING state may either complete successfully into the WAIT state or fail into the ABORT state
    # A process in the ABORT state exits with status>0
    if [ "$1" == "1" ]; then
      nwait=$(ls -1 ${workDir}/../proc_*/semaphore.WAIT 2> /dev/null | wc -l)
      if ls ${workDir}/../proc_*/semaphore.ABORT 2> /dev/null; then
        echo "[doOne ($1): syncprocesses] $(dateNS) semaphore master: one or more processes failed, send the ABORT signal"
        for procdir in ${workDir}/../proc_*; do touch $procdir/semaphore.ABORT; \rm -f $procdir/semaphore.WAIT; done
      elif [ "${nwait}" == "$NCOPIES" ]; then
        # NB! Use "$NCOPIES" instead of "$(ls -1 -d ${workDir}/../proc_* | wc -l)": some proc_* directories may not even exist yet!
        echo "[doOne ($1): syncprocesses] $(dateNS) semaphore master: all $NCOPIES processes are in the WAIT state, send the GO signal"
        gotime=$(dateNSepoch)
        for procdir in ${workDir}/../proc_*; do echo $gotime > $procdir/semaphore.GO; \rm -f $procdir/semaphore.WAIT; done
      else
        echo "[doOne ($1): syncprocesses] $(dateNS) semaphore master: ${nwait} processes out of $NCOPIES are in the WAIT state, keep waiting"
      fi
    fi
    if [ -f ${workDir}/semaphore.ABORT ]; then # NB this has priority over WAIT if both are present
      echo "[doOne ($1): syncprocesses] $(dateNS) semaphore.ABORT: skip further actions"
      status=2 # will break the action for-loop
      break # breaks the sleep while-loop 
    elif [ -f ${workDir}/semaphore.GO ]; then # NB this has priority over WAIT if both are present
      delay=$(( $(dateNSepoch) - $(cat ${workDir}/semaphore.GO) )) # this is useful for estimating the jitter in start times if needed...
      echo "[doOne ($1): syncprocesses] $(dateNS) semaphore.GO: start RUNNING a new action (delay from GO: $((delay/1000000000)).$((delay%1000000000)) seconds)"
      touch ${workDir}/semaphore.RUNNING
      \rm -f ${workDir}/semaphore.GO
      break # breaks the sleep while-loop 
    elif [ -f ${workDir}/semaphore.WAIT ]; then
      if [ $sleepstepsdone -eq 0 ]; then # reduce verbosity: print only once for now, eventually will improve
	echo "[doOne ($1): syncprocesses] $(dateNS) semaphore.WAIT: sleeping $sleepstep seconds (up to $sleepstepsleft times more)"
      fi
      (( sleepstepsleft -= 1 )); (( sleepstepsdone += 1 )); sleep $sleepstep
    else # THIS SHOULD NEVER HAPPEN!
      echo "[doOne ($1): syncprocesses] $(dateNS) semaphore.INTERNAL_ERROR (not ABORT, WAIT or GO): skip further actions"
      touch ${workDir}/semaphore.ABORT
      status=99 # will break the action for-loop
      break # breaks the sleep while-loop 
    fi
  done
  if [ $sleepstepsleft -le 0 ]; then
    echo "[doOne ($1): syncprocesses] $(dateNS) semaphore waited for too long, skip further actions and ABORT"; 
    touch ${workDir}/semaphore.ABORT
    status=3 # will break the action for-loop
  fi
  return $status
}

function clean_work_dir(){
  echo -e "\n[$bmkDriver] clean work directory ${baseWDir} with mode $MOP"
  echo "[$bmkDriver] Pre-cleaning size of ${baseWDir} "
  du -csh ${baseWDir}/*
  case "$MOP" in
    none )
      echo -e "\n[$bmkDriver:clean_work_dir] the $MOP mode was selected: nothing to do"
      return
      ;;  
    all )
      echo -e "\n[$bmkDriver:clean_work_dir] the $MOP mode was selected: removing everything"
      rm -rf ${baseWDir}/proc_*
      ;;
    custom )
      echo -e "\n[$bmkDriver:clean_work_dir] the $MOP mode was selected: calling function custom_clean_workdir"
      if [ "$(type -t custom_clean_workdir)" != "function" ]; then
        echo "[$bmkDriver:clean_work_dir] function 'custom_clean_workdir' is not defined in $bmkScript. Fallback to default custom $bmkDriver:clean_work_dir, i.e. removing root files"
        find ${baseWDir} -type f -name '*.root' -delete
      else
        custom_clean_workdir ${baseWDir}
      fi
      echo "[$bmkDriver:clean_work_dir] archiving proc_\*"
      tar -czf ${baseWDir}/archive_processes_logs.tgz ${baseWDir}/proc_*
      rm -rf ${baseWDir}/proc_*
      ;;
    * )
      echo -e "\n[$bmkDriver:clean_work_dir] the $MOP mode is not recognized."
      return
      ;;
  esac
  echo "[$bmkDriver:clean_work_dir] Post-cleaning size of ${baseWDir}"
  du -csh ${baseWDir}/*
}

function advertise_bmkdriver(){
  echo -e "\n========================================================================"
  echo -e "[$bmkDriver] $(date) entering common benchmark driver"
  echo -e "========================================================================\n"
  echo -e "[$bmkDriver] entering from $bmkScript\n"
  # Dump workload-specific directory
  echo -e "[$bmkDriver] benchmark directory BMKDIR=${BMKDIR}:\n"
  ls -lRt $BMKDIR
  if [ -d $BMKDIR/../data ]; then
    echo -e "\n[$bmkDriver] data directory ${BMKDIR}/../data:\n"
    ls -lRt $BMKDIR/../data
  fi
  echo
}

# Check that mandatory functions exist or load them otherwise
function check_mandatory_functions(){
  # Check that function doOne has been defined
  if [ "$(type -t doOne)" != "function" ]; then
    echo "[$bmkDriver] ERROR! Function 'doOne' must be defined in $bmkScript" # internal error (missing code)
    exit 1;
  fi
  # Check that function parseResults has been defined, otherwise load it from parseResults.sh
  if [ "$(type -t parseResults)" != "function" ]; then
    echo "[$bmkDriver] load parseResults.sh (function 'parseResults' is not defined in $bmkScript)"
    if [ -f ${BMKDIR}/parseResults.sh ]; then
      echo -e "[$bmkDriver] sourcing ${BMKDIR}/parseResults.sh\n"
      . ${BMKDIR}/parseResults.sh
      if [ "$(type -t parseResults)" != "function" ]; then
        echo "[$bmkDriver] ERROR! Function 'parseResults' must be defined in $bmkScript or parseResults.sh" # internal error (missing code)
        exit 1;
      fi
    else
      echo -e "[$bmkDriver] ERROR! 'parseResults' not defined and ${BMKDIR}/parseResults.sh not found\n" # internal error (missing code)
      exit 1
    fi
  fi
}

# Check that mandatory variables have been defined (default values)
function check_mandatory_variables(){
  # Variables NCOPIES, NTHREADS, NEVENTS_THREAD have default values specific to each benchmark
  for var in NCOPIES NTHREADS NEVENTS_THREAD; do # NB: EXTRA_ARGS is not a mandatory variable (BMK-1014)
    if [ "${!var}" == "" ]; then
      echo "[$bmkDriver] ERROR! A default value of $var must be set in $bmkScript" # internal error (missing code)
      exit 1;
    fi
  done
  echo
}

# Variables USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREAD, USER_EXTRA_ARGS are empty by default
USER_NCOPIES=
USER_NTHREADS=
USER_NEVENTS_THREADS=
USER_EXTRA_ARGS=
USER_N_TOTAL_CORES=


# Variable resultsDir has default value /results
# Variables skipSubDir and DEBUG are 0 by default
resultsDir=/results
skipSubDir=0
DEBUG=0
USE_PRMON=0
MOP="custom"

function advertise_user_defined_variables(){
  for var in NCOPIES NTHREADS NEVENTS_THREAD EXTRA_ARGS N_TOTAL_CORES; do
    echo "Default (from $bmkScript): $var='${!var}'"
  done
  echo
  for var in USER_NCOPIES USER_NTHREADS USER_NEVENTS_THREAD USER_EXTRA_ARGS USER_N_TOTAL_CORES; do
    echo "Default (from $bmkDriver): $var='${!var}'"
  done
  echo
  for var in resultsDir skipSubDir DEBUG MOP; do
    echo "Default (from $bmkDriver): $var='${!var}'"
  done
}

# Usage function
function usage(){
  echo ""
  echo "Usage: $0 [-w | --resultsdir <resultsDir>] [-W] [-c | --copies <NCOPIES>] [-t | --threads <NTHREADS>] "\
                 "[-e | --events <NEVENTS_PER_THREAD>] [-x | --extra-args <EXTRA_ARGS>] "\
                 "[-n | --ncores <N_TOTAL_CORES>] "\
                 "[-m | --mop <mode>] [-p | --prmon ] "\
                 "[-d | --debug] [-h | --help]"
  echo "  -w --resultsdir <resultsDir>   : (string) results directory (default: /results , current: $resultsDir)"
  echo "  -W                             : (bool) store results in <resultsDir> directly (default: 0 , current: $skipSubDir)"
  echo "  -c --copies <NCOPIES>          : (int) # identical copies (default $NCOPIES)"
  echo "  -t --threads <NTHREADS>        : (int) # threads (or processes, or threads*processes) per copy (default $NTHREADS)"
  echo "  -e --events <NEVENTS_THREAD>   : # events per thread (default $NEVENTS_THREAD)"
  echo "  -n --ncores <N_TOTAL_CORES>    : (int) total number of cores to load (will affect the computation of NCOPIES)"
  echo "  -x --extra-args '<EXTRA_ARGS>' : optional workload-specific command line arguments '<arg1> <arg2>...' (default '$EXTRA_ARGS')"
  echo "  -m --mop <mode>                : clean working directory mode: none/all/custom (current: $MOP)"
  echo "  -p --prmon                     : enable prmon for usage monitoring"  
  echo "  -d --debug                     : debug mode (current: $DEBUG)"
  echo "  -h --help                      : display this help and exit"
  echo ""
  if [ $NTHREADS -eq 1 ]; then
    echo "NTHREADS : the default value NTHREADS=1 of this parameter cannot be changed"
    echo "           (single-threaded single-process workload application)"
    echo ""
  fi
  echo "Mop mode: 
          none   == do not remove working files, 
          all    == remove all produced files (but summary json), 
          custom == custom implementation"
  echo ""
  echo "Without -W (default): results are stored in a new subdirectory of <resultsDir>:"
  echo "  <resultsDir>/<uniqueid>/*.json"
  echo "  <resultsDir>/<uniqueid>/proc_1/*.log"
  echo "  <resultsDir>/<uniqueid>/proc_.../*.log"
  echo "  <resultsDir>/<uniqueid>/proc_<COPIES>/*.log"
  echo ""
  echo "With -W (e.g. in the CI): results are stored in <resultsDir> directly:"
  echo "  <resultsDir>/*.json"
  echo "  <resultsDir>/proc_1/*.log"
  echo "  <resultsDir>/proc_.../*.log"
  echo "  <resultsDir>/proc_<NCOPIES>/*.log"
  echo ""
  echo "Without -w (default) and without -W: <resultsDir> is /results"
  echo "Without -w (default) and with -W: <resultsDir> is a tmp directory /tmp/xxxx"
  if [ "$(type -t usage_detailed)" == "function" ]; then
    echo -e "\nDetailed Usage:\n----------------\n"
    ( usage_detailed ) # as a subprocess, just in case this has a 0 exit code...
  fi
  echo -e "\nDESCRIPTION:\n----------------\n"
  if [ -e $BMKDIR/DESCRIPTION ]; then
    cat $BMKDIR/DESCRIPTION
  else
    echo "Sorry there is no description included!" # NB This cannot be made mandatory as this function exits an error anyway! BMK-1027
  fi
  echo ""
  echo -e "\nContainer disk space dump (folder threshold>500MB, max-depth=6):\n----------------\n"
  du --max-depth=6 -c -h -t 500000000 / 2> /dev/null
  echo -e "\n----------------\n"
  [[ "$callUsage"=="1" ]] && exit 0 # do not exit with failure status if --help requested
  exit 2 # early termination (help or invalid arguments to benchmark script)
}

#####################
### HERE MAIN STARTS
#####################

debug_args=$@
OPTPARSE=`getopt -o c:t:e:w:Wdhpm:x:n: --long help,debug,prmon,events:,threads:,copies:,mop:,extra-args:,ncores: -n $bmkScript -- "$@"`
if [ $? != 0 ] ; then echo "Invalid options provided." >&2 ; usage ; fi
eval set -- "$OPTPARSE"

# Parse the input arguments
callUsage==
while true; do
  case "$1" in
    -c | --copies )
      if [ $2 -gt 0 ]; then
        USER_NCOPIES=$2
      else
        echo "[$bmkDriver] ERROR! Invalid argument '-c $2' (must be > 0)"
        exit 1 # early termination (invalid arguments to benchmark script)
      fi
      shift 2
      ;;
    -t | --threads )
      if [ $2 -gt 0 ]; then
        USER_NTHREADS=$2
        if [ $NTHREADS -eq 1 ] && [ $USER_NTHREADS -ne 1 ]; then
          echo "[$bmkDriver] ERROR! Invalid argument '-t $2' (default NTHREADS=1 cannot be changed)"
          exit 1 # early termination (invalid arguments to benchmark script)
        fi
      else
        echo "[$bmkDriver] ERROR! Invalid argument '-t $2' (must be > 0)"
        exit 1 # early termination (invalid arguments to benchmark script)
      fi
      shift 2
      ;;
    -n | --ncores )
      if [ $2 -gt 0 ]; then
        USER_N_TOTAL_CORES=$2
      else
        echo "[$bmkDriver] ERROR! Invalid argument '-n | --ncores $2' (must be > 0)"
        exit 1 # early termination (invalid arguments to benchmark script)
      fi
      shift 2
      ;;
    -e | --events )
      if [ $2 -gt 0 ]; then
        USER_NEVENTS_THREAD=$2
      else
        echo "[$bmkDriver] ERROR! Invalid argument '-e $2' (must be > 0)"
        exit 1
      fi
      shift 2
      ;;
    -x | --extra-args )
      USER_EXTRA_ARGS="$2" # this may be a space-separated list of arguments (BMK-1014)
      shift 2
      ;;
    -w | --resultsdir )
      resultsDir=$2
      shift 2
      ;;
    -W )
      skipSubDir=1
      shift
      ;;
    -m | --mop )
      MOP=$2
      shift 2
      ;;
    -d | --debug )
      DEBUG=1
      shift
      ;;
    -p | --prmon )
      USE_PRMON=1
      shift
      ;;
    -h | --help )
      callUsage=1 # need to do in this way to enable parsing of all arguments (see BMK-258)
      shift
      ;;
    -- ) # getopt adds '--' as final arg marker
      shift
      break
      ;; 
    * )
      echo "ERROR: Unexpected option: $1\n"
      callUsage=1 # need to do in this way to enable parsing of all arguments (see BMK-258)
      shift
      ;;
  esac
done

# No other input arguments are expected
if [ "$1" != "" ]; then usage; fi

if [ "$callUsage" == "1" ]; then usage; fi

if [ "$DEBUG" == 1 ]; then
  echo -e "\n[$bmkDriver] Parsing input arguments '$debug_args'\n"
  advertise_bmkdriver
  advertise_user_defined_variables
fi

# include common functions for parsing
source ${BMKDIR}/parser-driver.sh

# Check that mandatory functions exist or load them otherwise
check_mandatory_functions

# Check that mandatory variables have been defined (default values)
check_mandatory_variables

# Dump all relevant variables after parsing the input arguments
for var in USER_NCOPIES USER_NTHREADS USER_NEVENTS_THREAD USER_EXTRA_ARGS USER_N_TOTAL_CORES; do
  echo "Current value: $var='${!var}'"
done
echo
for var in resultsDir skipSubDir DEBUG MOP; do
  echo "Current value: $var='${!var}'"
done
echo

# Variable resultsDir must be set through command line options
# Backward compatibility: all benchmarks initially hardcoded 'RESULTS_DIR=/results'
if [ "${resultsDir}" == "" ]; then
  ###echo "[$bmkDriver] ERROR! resultsDir not specified ('-w' missing)"
  ###exit 1 # early termination (invalid arguments to benchmark script)
  if [ "$skipSubDir" == "1" ]; then
    echo -e "[$bmkDriver] WARNING! resultsDir not specified ('-w' missing), but '-W' is present: create a directory in /tmp\n"
    resultsDir=$(mktemp -d)
  else
    echo -e "[$bmkDriver] WARNING! resultsDir not specified ('-w' missing) and '-W' is missing: assume '/results'\n"
    resultsDir=/results
  fi
fi

# Check that resultsDir is an existing directory
if [ ! -d ${resultsDir} ]; then
  mkdir -p ${resultsDir}
  if [ "$?" != "0" ]; then
    echo "[$bmkDriver] ERROR! directory '${resultsDir}' not found and could not be created"
    exit 1 # early termination (cannot start processing)
  fi
fi

# Status code of the validateInputArguments and doOne steps
# fail<0 : validateInputArguments failed
# fail>0 : doOne failed
# fail=0 : OK
fail=0

# Call function validateInputArguments if it exists
if [ "$(type -t validateInputArguments)" != "function" ]; then
  echo -e "[$bmkDriver] function 'validateInputArguments' not found: use input arguments as given\n"
  if [ "$USER_NCOPIES" != "" ]; then NCOPIES=$USER_NCOPIES; fi
  if [ "$USER_NTHREADS" != "" ]; then NTHREADS=$USER_NTHREADS; fi # already checked that USER_NTHREADS must be 1 if NTHREADS is 1
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  if [ "$USER_EXTRA_ARGS" != "" ]; then EXTRA_ARGS="$USER_EXTRA_ARGS"; fi
else
  echo -e "[$bmkDriver] function 'validateInputArguments' starting\n"
  if ! validateInputArguments; then fail=-1; fi
  echo -e "\n[$bmkDriver] function 'validateInputArguments' completed (status=$fail)\n"
fi

# if a max number of cores is configured to be loaded then compute how many copies are needed
# to fill exactly that number of cores. 
if [ "$USER_N_TOTAL_CORES" != "" ]; then N_TOTAL_CORES=$USER_N_TOTAL_CORES; fi
if [ "$N_TOTAL_CORES" != "" ]; then  #i.e. if defined via USER_N_TOTAL_CORES or hardcoded default value
  echo -e "[$bmkDriver] Eximining the request to load a custom number of cores ($N_TOTAL_CORES)..."
  if [ $(($N_TOTAL_CORES%$NTHREADS)) -eq 0 ]; then
    NCOPIES=$((N_TOTAL_CORES/NTHREADS)); 
    echo -e "[$bmkDriver] Request accepted: reducing the number of copies consistently NCOPIES = N_TOTAL_CORES/NTHREADS = ${N_TOTAL_CORES}/${NTHREADS} = $NCOPIES"
  else
    echo -e "[$bmkDriver] ERROR! This run uses threads=$NTHREADS per copy and can not fill ncores=$N_TOTAL_CORES cores with an integer number of copies. Please change accordingly the number of threads (-t | --threads) if -ncopies is used\n"
    exit 1
  fi
else 
  # if N_TOTAL_CORES is not defined, define it as NTHREADS*NCOPIES. Needed for the parser report
  N_TOTAL_CORES=$((NCOPIES*NTHREADS));
  echo -e "[$bmkDriver] The undefined N_TOTAL_CORES is fixed to NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=${N_TOTAL_CORES}"
fi

# Set baseWDir and create it if necessary
if [ "$skipSubDir" == "1" ]; then
  baseWDir=${resultsDir}
  echo -e "[$bmkDriver] base working directory : $baseWDir\n"
else
  baseWDir=${resultsDir}/$(basename $0 -bmk.sh)-c${NCOPIES}-t${NTHREADS}-e${NEVENTS_THREAD}-$(date +%s)_$(((RANDOM%9000)+1000))
  echo -e "[$bmkDriver] base working directory : $baseWDir\n"
  if ! mkdir $baseWDir; then
    echo "[$bmkDriver] ERROR! directory '${baseWDir}' cannot be created"
    exit 1 # early termination (cannot start processing)
  fi
fi
baseWDir=$(cd $baseWDir; pwd)
export baseWDir
# Dump all relevant variables after validating the input arguments
# Keep a copy on a separate log too for parser tests on previous logs
touch $baseWDir/inputs.log
for var in NCOPIES NTHREADS NEVENTS_THREAD; do
  if [ "${!var}" == "" ] || ! [[ ${!var} =~ ^[0-9]+$ ]] || [ ! ${!var} -gt 0 ]; then
    echo "[$bmkDriver] ERROR! Invalid value $var=${!var}"
    exit 1;
  fi
  echo "Current value: $var=${!var}"
  echo "$var=${!var}" >> $baseWDir/inputs.log
done
for var in EXTRA_ARGS N_TOTAL_CORES; do
  echo "Current value: $var='${!var}'"
  echo "$var='${!var}'" >> $baseWDir/inputs.log # this is a string not a number (BMK-1014 and BMK-1029)
done
echo

# Keep a copy of the version.json file (in gitlab CI artifacts) for parser tests on previous logs
# NB: DO NOT REMOVE THIS! This is needed to fix BMK-1008 (see also BMK-1006 and BMK-1009)
if [ -f $BMKDIR/version.json ]; then
  cp $BMKDIR/version.json $baseWDir
fi

# Add the containement info in the version.json file
if ! enrich_version_json; then # add error checking to jq (BMK-1022)
  exit 1 # early termination (cannot start processing)
fi

# Define APP before doOne (BMK-152) and parseResults
APP=$(basename ${BMKDIR}) # or equivalently here $(basename $0 -bmk.sh)
echo -e "[$bmkDriver] APP=${APP}\n"

# Wrapper for the doOne function
function doOneWrapper(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then
    echo -e "[$bmkDriver] ERROR! Invalid arguments '$@' to doOneWrapper" # internal error (inconsistent code)
    return 1 # NB: return or exit are equivalent here because doOneWrapper is executed as a subprocess
  fi
  echo -e "\n[doOneWrapper ($1)] $(date) : process $1 started"
  workDir=$(pwd)/proc_$1 # current directory is $baseWDir here
  echo -e "[doOneWrapper ($1)] workdir is ${workDir}"
  if ! mkdir -p $workDir || ! cd $workDir; then
    echo -e "\n[doOneWrapper ($1)] $(date) : process $1 failed (cannot create workdir)\n"
    return 1
  fi
  log=${workDir}/doOneWrapper_$1.log
  echo -e "[doOneWrapper ($1)] logfile is $log"
  if ! touch $log ; then
    echo -e "\n[doOneWrapper ($1)] $(date) : process $1 failed (cannot create logfile)\n"
    return 1
  fi
  echo -e "[doOneWrapper ($1)] $(date) : process $1 configured" 2>&1 | tee -a $log # configured means that log exists
  mkdir $workDir/HOME
  export HOME=$workDir/HOME # avoid writing to /root in read-only docker or to host HOME in singularity (BMK-166)
  echo -e "[doOneWrapper ($1)] HOME=$HOME" 2>&1 | tee -a $log
  cd -P /proc/self && basename $PWD | ( read thispid; \
    echo -e "[doOneWrapper ($1)] current process pid is $thispid" 2>&1 | tee -a $log ) # see https://stackoverflow.com/a/15170225
  cd - > /dev/null
  local pid=$(cat $log | grep "current process pid is" | sed -e "s/.*current process pid is //")
  local parsertest=0 # hardcoded: 0 => doOne (default); 1 => test the parser on old logs and bypass doOne (BMK-152)
  if [ $parsertest -eq 0 ]; then
    echo -e "[doOneWrapper ($1)] run doOne as $(whoami) in $(pwd)\n" 2>&1 | tee -a $log
    doOne $1 2>&1 | tee -a $log
    local status=${PIPESTATUS[0]} # NB do not use $? if you pipe to tee!
  else
    cp -dpr $BMKDIR/jobs/refjob/proc_$1/* .
    local status=$?
    \rm -f *${APP}*.json
    echo -e "[doOneWrapper ($1)] DUMMY doOne: copy old logs for parser tests (BMK-152)"
  fi
  if [ "$status" == "0" ]; then
    echo -e "\n[doOneWrapper ($1)] $(date) : process $1 (pid=$pid) completed ok\n" 2>&1 | tee -a $log
    return 0
  else
    echo -e "\n[doOneWrapper ($1)] $(date) : process $1 (pid=$pid) failed\n" 2>&1 | tee -a $log
    return 1
  fi
}

# Export variables to the doOne subprocesses
for var in NCOPIES NTHREADS NEVENTS_THREAD EXTRA_ARGS N_TOTAL_CORES BMKDIR DEBUG USE_PRMON APP; do
  export $var
done

# Spawn doOne subprocesses (unless validateInputArguments failed)
if [ $fail -eq 0 ]; then

  # Spawn subprocesses (and keep track of their list of them using '$!')
  echo -e "------------------------------------------------------------------------"
  echo -e "[$bmkDriver] spawn $NCOPIES processes"
  echo -e "------------------------------------------------------------------------\n"
  jobs=""
  for i in $(seq 1 $NCOPIES); do
    ( cd $baseWDir; doOneWrapper $i ) &
    ipid=$!
    [ $DEBUG -gt 0 ] && echo -e "[$bmkDriver] spawned process $i with pid $ipid"
    if [ ${USE_PRMON} -eq 1 ] ; then
      ( mkdir -p $baseWDir/prmon_res/prmon_$i;
        cd $baseWDir/prmon_res/prmon_$i;
        prmon -i 5 --pid $ipid &> prmon.out ) &
      ppid=$!
      jobs="$jobs $ppid"
    fi
    jobs="$jobs $ipid"
    sleep 0.1 # stagger job creation by 100ms
  done

  # Wait for all subprocesses to complete and check their exit codes
  # [NB: do not use 'jobs -p': some jobs may be missing if already completed]
  [ $DEBUG -gt 0 ] && echo -e "\n[$bmkDriver] $(date) ... waiting for spawned processes with pid's$jobs\n"
  wait $jobs > /dev/null 2>&1
  fail=0 # unnecessary but harmless (this code is only executed if $fail -eq 0)
  for i in $(seq 1 $NCOPIES); do
    if [ $(cat $baseWDir/proc_$i/doOneWrapper_$i.log | grep "[doOneWrapper ($i)]" | grep "completed ok" | wc -l) -ne 1 ]; then
      let "fail+=1"
    fi
  done
  echo -e "\n------------------------------------------------------------------------"
  if [ $fail -gt 0 ]; then
    echo "[$bmkDriver] ERROR! $fail processes failed (out of $NCOPIES)"
  else
    echo "[$bmkDriver] all $NCOPIES processes completed successfully"
  fi
  echo -e "------------------------------------------------------------------------\n"

  # If a build directory exists for process 1, move it to /results so that it can be exported (BMK-779)
  # Otherwise, create an empty /results/build directory
  if [ -d $baseWDir/proc_1/build ]; then
    echo "[$bmkDriver] move $baseWDir/proc_1/build to ${resultsDir}/build"
    \mv $baseWDir/proc_1/build ${resultsDir}/build
  else
    echo "[$bmkDriver] directory $baseWDir/proc_1/build not found - no need to move it"
    mkdir -p ${resultsDir}/build/.keepme2 # use "mkdir -p" to fix "keepme2 file exists" (BMK-1020)
  fi
  echo -e "------------------------------------------------------------------------\n"

# Skip the doOne step if validateInputArguments failed
else
  echo -e "[$bmkDriver] validateInputArguments failed: skip doOne processing"
fi

# Parse results and generate summary using function parseResults
# - parseResults is started in the base working directoy
# - the number of failed jobs is passed to parseResults as input parameter
# - if a separate function generateSummary exists, it must be internally called by parseResults
# - the environment variable APP=<vo>-<workload> defines the name of the json file ${APP}_summary.json
cd $baseWDir
echo -e "[$bmkDriver] parse results and generate summary: starting"
echo -e "[$bmkDriver] current directory : $(pwd)\n"

parseResultsWrapper $fail
parse=$?
echo -e "\n[$bmkDriver] parse results and generate summary: completed (status=$parse)"

# Validate json files syntax (BMK-137)
cd $baseWDir
echo -e "\n[$bmkDriver] json file validation: starting"
json=0
jsonFile=$baseWDir/${APP}_summary.json
if [ ! -f ${jsonFile} ]; then
  echo -e "[$bmkDriver] ERROR! json file '${jsonFile}' not found"
  json=1
else
  echo "[$bmkDriver] lint json file '${jsonFile}' syntax using jq"
  if ! jq '.' -c < ${jsonFile}; then
    echo "[$bmkDriver] json file '${jsonFile}' lint validation failed. Copying file to ${jsonFile}_fail_lint"
    mv ${jsonFile} ${jsonFile}_fail_lint
    failed_json=`cat ${jsonFile}_fail_lint`
    echo '{}' | jq --arg fjson "$failed_json" '. + {"not_a_json": $fjson}' > ${jsonFile}
    json=1
  fi
fi
echo -e "[$bmkDriver] json file validation: completed (status=$json)\n"

# Last step before exiting: clean up work directories
clean_work_dir

# NB: This script is meant to be sourced, it does not return or exit at the end
if [ $parse -ne 0 ] || [ $fail -ne 0 ] || [ $json -ne 0 ]; then
  bmkStatus=1
else
  bmkStatus=0
fi
echo -e "[$bmkDriver] exiting back to $bmkScript"
echo -e "\n========================================================================"
echo -e "[$bmkDriver] $(date) exiting common benchmark driver (status=$bmkStatus)"
echo -e "========================================================================\n"
exit $bmkStatus
