HEPWL_BMKEXE=cms-reco-run3-ma-bmk.sh
HEPWL_BMKOPTS="-t 4 -e 25 -c 1" 
HEPWL_BMKDIR=cms-reco-run3-ma
HEPWL_BMKDESCRIPTION="CMS RECO of ttbar events, based on CMSSW_12_5_0"
HEPWL_DOCKERIMAGENAME=cms-reco-run3-ma-bmk
HEPWL_DOCKERIMAGETAG=v1.2
HEPWL_CVMFSREPOS=cms.cern.ch
HEPWL_BMKOS="cern/cs8-base:20220901-1"
HEPWL_BUILDARCH="x86_64,aarch64"
