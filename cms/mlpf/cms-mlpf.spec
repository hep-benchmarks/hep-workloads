HEPWL_BMKEXE=cms-mlpf-bmk.sh
# docker may run out of shared memory for events > 1000, for CPU-only
# critically, reduce number of convolutions to 1 for CPU CI
HEPWL_BMKOPTS="-x '--ntrain 300 --nvalid 300 --gpus 1 --num-epochs 2 --num-convs 1'"
HEPWL_BMKDIR="cms-mlpf"
HEPWL_BMKDESCRIPTION="CMS Machine-Learned ParticleFlow (MLPF)"
HEPWL_DOCKERIMAGENAME=cms-mlpf-bmk
HEPWL_DOCKERIMAGETAG=ci-v0.4 # NB ci-vX.Y for tests, vX.Y for tags
HEPWL_CVMFSREPOS=NONE
HEPWL_BMKOS="gitlab-registry.cern.ch/linuxsupport/alma9-base:latest"
HEPWL_BMKUSEGPU=1
HEPWL_BUILDARCH="x86_64"
