#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
parseResults.py

This is meant to be called from the parser script `parseResults.sh`

"""
import argparse
import json
from pathlib import Path
import yaml

directory = "history"  # replace with the desired directory

wl_stats = {}
wl_scores = {}
epoch_times = []

def parse_results(results_dir):
    """results_dir is a path-like object to the directory containing a train-config yaml and history folder."""
    results_dir=Path(results_dir)

    if not results_dir.is_dir():
        raise ValueError(f"{results_dir} is not a directory")
    
    # check if passed $baseWDir or $workDir
    results_dir = results_dir.joinpath("proc_1")

    with results_dir.joinpath("train-config.yaml").open(encoding="utf-8") as file:
        config = yaml.safe_load(file)

    wl_stats["gpus"] = config["gpus"]
    wl_stats["dtype"] = config["dtype"]
    wl_stats["events"] = config["ntrain"]
    wl_stats["gpu_batch_multiplier"] = config["gpu_batch_multiplier"]
    wl_stats["batch_size"] = config["train_dataset"][config["dataset"]]["physical"]["batch_size"]
    wl_stats["events_per_batch"] = (
        wl_stats["gpu_batch_multiplier"] * wl_stats["batch_size"]
        if wl_stats["gpus"] > 0
        else wl_stats["batch_size"]
    )
    wl_stats["conv_type"] = config["conv_type"]
    wl_stats["dataset"] = config["train_dataset"][config["dataset"]]["physical"]["samples"]

    # parse all results json
    for filepath in results_dir.joinpath(directory).rglob("*.json"):
        with filepath.open() as file:
            data = json.load(file)
            if "epoch_train_time" in data:
                epoch_times.append(round(data["epoch_train_time"], 4))

    wl_stats["num_epochs"] = len(epoch_times)
    wl_stats["epoch_times"] = sorted(epoch_times, reverse=True)
    wl_stats["train_time"] = round(sum(epoch_times), 4)
    wl_stats["throughput_per_epoch"] = [round(wl_stats["events"] / t, 4) for t in wl_stats["epoch_times"]]

    wl_scores["mean_throughput"] = round(sum(wl_stats["throughput_per_epoch"][1:])/(wl_stats["num_epochs"] -1 ), 4)
    wl_scores["mean_epoch_time"] = round(sum(wl_stats["epoch_times"][1:])/(wl_stats["num_epochs"] -1 ), 4)

    report = {"wl-stats": wl_stats, "wl-scores": wl_scores}
    print(json.dumps(report, indent=4))

if __name__ == "__main__":
    parser=argparse.ArgumentParser()
    parser.add_argument("results_dir", type=Path, help="path to results directory")
    args=parser.parse_args()

    parse_results(args.results_dir)
