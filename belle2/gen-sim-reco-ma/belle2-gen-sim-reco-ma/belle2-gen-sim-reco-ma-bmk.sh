#!/bin/bash

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  echo "[doOne ($1)] do one! (process $1 of $NCOPIES)"

  ARCHSTRING=`uname -p` 
  echo "Detected CPU architecture $ARCHSTRING" 
  if [[ ${ARCHSTRING} =~ (^x86_64$) ]]; then   
    export ARCH=x86
  elif [[ ${ARCHSTRING} =~ (^aarch64$) ]]; then   
    export ARCH=aarch64
  else     
    echo "ERROR! This script is only supported on x86 and aarch64 architectures"     
    exit 1 
  fi  

  curdir=`pwd`
  export BELLE2_CONDB_SERVERLIST=/cvmfs/belle.cern.ch/conditions/database.sqlite
  export BELLE2_NO_TOOLS_CHECK=True

  if [ $ARCH = "x86" ]; then
    # Configure WL copy
    cat > SConscript <<EOT
Import('env')
# This file specifies the dependencies of your Analyis code to parts of the
# Belle 2 Software. It should be fine for most analysis but if you need to link
# against additional libraries pleas put them here.
env['LIBS'] = [
    'mdst_dataobjects',
    'analysis_dataobjects',
    'analysis',
    'framework',
    '\$ROOT_LIBS',
]
Return('env')
EOT
    
    #echo release-05-01-22 > .analysis 
    echo release-06-00-08 > .analysis
    # Suppress unnecessary warning message
    source /cvmfs/belle.cern.ch/tools/b2setup release-06-00-08 > /dev/null
  else
    export DYLD_LIBRARY_PATH=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/root/lib
    export PATH=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/root/bin:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/bin:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/bin:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/bin:/cvmfs/belle.cern.ch/arm/releases/release-06-00-08/bin/Linux_aarch64/opt:/cvmfs/belle.cern.ch/tools:/usr/lib64/qt-3.3/bin:/usr/lib64/ccache:$PATH
    export ROOT_INCLUDE_PATH=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/include/libxml2:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/include:/cvmfs/belle.cern.ch/arm/externals/v01-10-02:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/include/python3.8:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/include/CLHEP:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/include/Geant4:/cvmfs/belle.cern.ch/arm/releases/release-06-00-08/include:/cvmfs/belle.cern.ch/arm/releases/release-06-00-08
    export LIBPATH=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/root/lib
    export GXX_INCLUDE=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/include/c++
    export BELLE2_RELEASE_DIR=/cvmfs/belle.cern.ch/arm/releases/release-06-00-08/
    export BELLE2_EXTERNALS_SUBDIR=Linux_aarch64/common
    export BELLE2_EXTERNALS_OPTION=common
    export BELLE2_EXTERNALS_VERSION=v01-10-02
    export BELLE2_SUBDIR=Linux_aarch64/opt
    export BELLE2_EXTERNALS_DIR=/cvmfs/belle.cern.ch/arm/externals/v01-10-02
    export BELLE2_ARCH=Linux_aarch64
    export VO_BELLE2_SW_DIR=/cvmfs/belle.cern.ch/arm/
    export BELLE2_RELEASE=release-06-00-08
    export BELLE2_EXTERNALS_TOPDIR=/cvmfs/belle.cern.ch/arm/externals
    export BELLE2_TOOLS=/cvmfs/belle.cern.ch/tools
    export PATH="$PATH:/cvmfs/belle.cern.ch/arm/releases/release-06-00-08/bin/Linux_aarch64/opt/"
    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/root/lib:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/lib64:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/lib64:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/lib:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/epics/lib/linux-aarch64:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/lib64:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/lib:/cvmfs/belle.cern.ch/arm/releases/release-06-00-08/lib/Linux_aarch64/opt:/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/lib"
    export PYTHONPATH=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/root/lib:/cvmfs/belle.cern.ch/arm/releases/release-06-00-08/lib/Linux_aarch64/opt:/cvmfs/belle.cern.ch/tools
    export PYTHIA8DATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/share/Pythia8/xmldoc

    export G4LEVELGAMMADATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/PhotonEvaporation5.5
    export G4LEDATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/G4EMLOW7.9.1
    export G4NEUTRONHPDATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/G4NDL4.6
    export G4ENSDFSTATEDATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/G4ENSDFSTATE2.2
    export G4RADIOACTIVEDATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/RadioactiveDecay5.4
    export G4ABLADATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/G4ABLA3.1
    export G4PIIDATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/G4PII1.3
    export G4PARTICLEXSDATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/G4PARTICLEXS2.1
    export G4SAIDXSDATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/G4SAIDDATA2.0
    export G4REALSURFACEDATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/RealSurface2.1.1
    export G4INCLDATA=/cvmfs/belle.cern.ch/arm/externals/v01-10-02/Linux_aarch64/common/share/Geant4-10.6.3/data/G4INCL1.0
  fi

  ln -s $BMKDIR/bmk.py bmk.py

  # Execute WL copy
  echo "Executing the following number of threads:"
  echo $NTHREADS

  # Ignore requests for multi-threading
  basf2 bmk.py -n $(( $NEVENTS_THREAD ))  >& $curdir/output
  status=$?

  # Set to 1 to print workload output to screen
  debug=0
  if [ $debug -eq 1 ]; then
    cd $curdir;
    echo "_________________________________________________OUTPUT START_________________________________________________";
    cat output;
    echo "_________________________________________________OUTPUT END_________________________________________________";
  fi 

  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Dummy version: accept user inputs as they are
  if [ "$USER_NCOPIES" != "" ]; then NCOPIES=$USER_NCOPIES; fi
  if [ "$USER_NTHREADS" != "" ]; then NTHREADS=$USER_NTHREADS; fi
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  # Return 0 if input arguments are valid, 1 otherwise
  return 0
}

# Optional function usage_detailed may be defined in each benchmark
# Input arguments: none
# Return value: none
function usage_detailed(){
  echo "NCOPIES*NTHREADS may be lower or greater than nproc=$(nproc)"
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=50

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
