#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  sleep 5s 
  echo "Hello world" > out_$1.log 2>&1
  echo "[doOne ($1)] $(date) EXTRA_ARGS='$EXTRA_ARGS'"
  echo "Starting ($1) with $EXTRA_ARGS" > out_$1.log
  echo "[doOne ($1)] $(date) resultsDir='$resultsDir'"

  # parse extra args if any

  # handle inherited args as well-- (-d --debug  $DEBUG)

  # options=$(getopt -a -n cms-mlpf-bmk -o g:D:B: --long nepochs:,ntrain:,ntest:,nvalid:,batch-multiplier:,gpus:,dtype:nworkers: -- "$EXTRA_ARGS")
  # eval set -- "$options"
  # while [ : ]; do
  #   case "$1" in
  #     --ntrain ) NTRAIN="$2"; shift;;
  #     --ntest ) NTEST="$2"; shift;;
  #     --nvalid ) NVALID="$2"; shift;;
  #     --nepochs ) NEPOCHS="$2"; shift;;
  #     --nworkers ) NWORKERS="$2"; shift;;
  #     --gpus | -g ) NGPUS="$2"; shift;;
  #     --gpu-batch-multiplier | -B ) BSIZE="$2"; shift;;
  #     --dtype | -D ) DTYPE="$2"; shift;;
  #     --train ) TRAIN="--train";;
  #     -- ) shift; break;;
  #   esac
  #   shift
  # done

  # Apparently the workload is now called from the results dir???
  pwd
  cd /bmk/cms-mlpf

  # Run the workload
  python3 mlpf/pyg_pipeline.py ${TRAIN} \
  --config parameters/pytorch/pyg-clic-bmk.yaml \
  --benchmark \
  --experiments-dir $workDir \
  ${EXTRA_ARGS}
  # --gpus $NGPUS \
  # --gpu-batch-multiplier $BSIZE \
  # --num-epochs $NEPOCHS \
  # --ntrain $NTRAIN \
  # --ntest $NTEST \
  # --nvalid $NVALID \
  # --dtype $DTYPE \
  # $TRAIN
  

  #--prefix /tmp/train_ \
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

function parseResults(){
  echo "[parseResults] Parsing results from baseWDir=$baseWDir"
  resJSON=$(python3 /bmk/cms-mlpf/parseResults.py $baseWDir)
  pystatus=$?
  if [ "$pystatus" == "0" ]; then
    echo $resJSON > $baseWDir/parser_output.json
    cat $baseWDir/parser_output.json
  fi
  echo "[parseResults] python parser completed (status=$pystatus)"
  return $pystatus
}
# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=1 # cannot be changed by user input ()
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=1 # not relevant for GPUs
# specific to MLPF
NEPOCHS=6     # must be >1 as 1st epoch is thrown away
NTRAIN=120000 # 0 is None, events to train on
NTEST=36000   # 0 is None, events to test training
BSIZE=8       # 8 is Default, batch size (too small device is under-loaded, too large OOM)
NGPUS=1    # 0 is Default, GPUs
TRAIN="--train"
DTYPE="bfloat16"  # float32, float16, bf16
DEBUG=0
#resultsDir="/results"

function usage_detailed(){
  echo ""
  echo "Additional MLPF parameters: use -x '<EXTRA_ARGS>'"
  echo "     --num-epochs              : (int) Number of epochs >1 (default: $NEPOCHS)"
  echo "     --ntrain                  : (int) Train steps limit (default: $NTRAIN)"
  echo "     --ntest                   : (int) Test steps limit (default: $NTEST)"
  echo "     --nvalid                  : (int) Validation steps limit (default: $NVALID)"
  echo "     --gpu-batch-multiplier    : (int) Increases GPU batch size by constant multiplier 1=1G, 8=10G (default: $BSIZE)"
  echo "     --dtype                   : (string) Data type {float32, float16, bfloat16}(default: $DTYPE)"
  echo "     --gpus                    : (int) Number of gpus to use (default: $NGPUS)"
  
}


if [ -f /run/.containerenv ]; then FLAVOR="podman"
elif [ -f /.dockerenv ]; then FLAVOR="docker"
elif [ -f /singularity ]; then FLAVOR="singularity"
else FLAVOR="unknown";
fi

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
